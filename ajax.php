<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");

if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/estilo.css" rel="stylesheet" type="text/css">
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" ></script>
        <script src="js/myJs.js"></script>      
        <script src="bootstrap/dist/js/bootstrap-checkbox.min.js" defer></script>        
        <script type="text/javascript" src="bootstrap/moment/moment.js"></script>
        <script type="text/javascript" src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/transition.js"></script>
        <script type="text/javascript" src="bootstrap/js/collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {                
                $('#material').load('material.php');
                $('#usuarios').load('registro.php');
                $('#contenedores').load('contenidors.php');
                
            });
        </script>

    </head>
    <body>   
        <!--HEADER-->
        <div class="banner">        
            <?php
            echo 'Bienvenido, ' . utf8_encode($_SESSION['nom']) . ' ' . utf8_encode($_SESSION['cognom']);
            ?>
        </div>        
        <!--TRY-->
        <nav class="nav nav-tabs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#material" data-toggle="tab">Material</a></li>                        
                        <li><a href="#usuarios" data-toggle="tab">Usuaris</a></li>
                        <li><a href="#contenedores" data-toggle="tab">Contenidors</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--FIN HEADER-->

        <div class="tab-content" >
            <div class="tab-pane active" id="material">
            </div>
            <div class="tab-pane" id="usuarios">
            </div>
            <div class="tab-pane" id="contenedores">
            </div>
        </div>
    </body>
</html>