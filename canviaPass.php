<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");

if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>GIET - Canvia Contrasenya</title>
        <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
        <link href="css/estilo.css" rel="stylesheet" type="text/css">
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" ></script>
        <script src="js/myJs.js"></script>      
        <script src="bootstrap/dist/js/bootstrap-checkbox.min.js" defer></script>        
        <script type="text/javascript" src="bootstrap/moment/moment.js"></script>
        <script type="text/javascript" src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/transition.js"></script>
        <script type="text/javascript" src="bootstrap/js/collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script src="js/myJs.js"></script>  
        <script type="text/javascript">
            $(document).ready(function () {
                //CARGAR COMBO PERMISOS
                comboPermisos();

                //PREPARA EVENTO DESCRIPCIONES PERMISOS
                $("#permisos").change(function () {
                    getComentarioXId();
                });

                $("#registra").click(function () {
                    registraUsuario();
                });

            });
        </script>
    </head>
    <body>    
        <!--HEADER-->
        <div class="banner">
            <div class="bienvenida">
                <?php
                echo 'Benvingut, ' . utf8_encode($_SESSION['nom']) . ' ' . utf8_encode($_SESSION['cognom']);
                if (isset($_SESSION['noHash'])) {
                    echo "<div class='alert alert-danger' role='alert'>CONTRASEÑA NO HASHEADA</div>";
                }
                ?>
            </div>
            <div>        
                <img src="img/giet.png"/>
            </div>      
        </div>
        <nav class="nav nav-tabs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="material.php"><span>Material</span></a></li>                        
                        <li class="altaUsuario active"><a href="registro.php"><span>Usuaris</span></a></li>
                        <li><a href="contenidors.php"><span>Contenidors</span></a></li>
                        <li><a href="AsignarMaterial.php"><span>Assignar Contenidors</span></a></li>
                    </ul>
                    <div class="pull-right">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> El meu compte<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="canviaPass.php"><i class="icon-cog"></i>Canviar la contrasenya</a></li>
                                    <li class="divider"></li>
                                    <li><a href="tancaSessio.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Tancar la sessió</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--FIN HEADER-->

        <div class="contenido col-md-12">   
            <div class="col-md-12">
                <div id="infoRegistro"></div>
                <div class="form-group">
                    <label>Nom</label>
                    <input class="form-control col-md-9" type="text" name="userName" id="nameRegis" required>                            
                </div>
                <div class="form-group">
                    <label>Cognoms</label>
                    <input class="form-control"type="text" name="lastNameRegis" id="lastNameRegis" required>                         
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control"type="text" name="emailRegis" id="emailRegis" required>                          
                </div>
                <div class="form-group">
                    <label>Permisos</label>
                    <select id="permisos" class="form-control" name="perm" class="col-md-12">
                    </select>                
                </div>
                <div class="form-group">
                    <div id="comentario" class="alert alert-warning" role="alert">Insertado</div>
                </div>

                <div>
                    <button id="registra" name="registra" class="btn btn-primary">Registrar usuari</button>
                </div>
            </div>
        </div>
    </body>
</html>
