<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");
if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>GIET - Material</title>
        <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/estilo.css" rel="stylesheet" type="text/css">
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" ></script>
        <script src="js/myJs.js"></script>      
        <script src="bootstrap/dist/js/bootstrap-checkbox.min.js" defer></script>        
        <script type="text/javascript" src="bootstrap/moment/moment.js"></script>
        <script type="text/javascript" src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/transition.js"></script>
        <script type="text/javascript" src="bootstrap/js/collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script src="js/myJs.js"></script>  
        <script type="text/javascript">
            $(document).ready(function () {
                //CARGAR COMBOS
                cambioDepartamento();
                comboProveedor();
                comboTipusMaterial();
                $("#observacionsMaterialInsert").val("");

                //PREPARAR BOTON BUSQUEDA TABLA
                $(".search").keyup(function () {
                    busquedaTabla(this)
                });

                //PREPARA BOTONES MODALES
                $("#saveImage").click(function () {
                    modificaMaterial();
                });
                $("#btnAnadirMaterial").click(function () {
                    insertaMaterial();
                });
                $("#btnEliminarMaterial").click(function () {
                    eliminaMaterial();
                });
                $("#btnBuscar").click(function () {
                    $("#infoDiv").html("");
                });
                $("#siTancaSessio").click(function () {
                    window.location.href = "tancaSessio.php";
                });
                $("#canviaContrasenya").click(function () {
                    cambiarContrasena();
                });

            });
        </script>
    </head>
    <body>
        <!--MODALES--> 
        <div class="modal fade" id="insertaMaterial" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Afegir Material</h3>
                    </div>
                    <div>
                        <form id="formModal" method="post">
                            <input id="idInsert" type="hidden"  name="idInsert"/>
                            <div class="form-group col-md-12 col-sm-12 firstGroup">
                                <label class="col-md-2 col-sm-2">Nom</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="text"  id="nomMaterialInsert"  name="nomMaterialInsert">                                    
                                </div>
                                <label class="col-md-2 col-sm-2" >Descripció</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control col-md-4 col-sm-4" type="text" id="descripcioMaterialInsert"  name="descripcioMaterialInsert">                 
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Número de serie</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="numSerieMaterialInsert" name="numSerieMaterialInsert">                             
                                </div>
                                <label class="col-md-2 col-sm-2">Marca</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="marcaMaterialInsert" name="marcaMaterialInsert">           
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Model</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="modelMaterialInsert" name="modelMaterialInsert">                         
                                </div>
                                <label class="col-md-2 col-sm-2">Tipus</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="tipusMaterialInsert" name="tipusMaterialInsert"> </select>    
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Inventariable</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="checkbox" id="inventariableMaterialInsert" name="inventariableMaterialInsert">                  
                                </div>
                                <label class="col-md-2 col-sm-2">Identificador</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="identificadorMaterialInsert" name="identificadorMaterialInsert">
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data caducitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadMaterialInsert" name="dataCadMaterialInsert"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-md-2 col-sm-2">Valor per unitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="valorMaterialInsert" name="valorMaterialInsert">
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat inicial</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatIniMaterialInsert" name="quantitatIniMaterialInsert">        
                                </div>
                                <label class="col-md-2 col-sm-2">Quantitat actual</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatActMaterialInsert" name="quantitatActMaterialInsert">        
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat mínima</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatMinMaterialInsert" name="quantitatMinMaterialInsert">
                                </div>
                                <label class="col-md-2 col-sm-2">Proveidor</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="proveidorMaterialInsert"  name="proveidorMaterialInsert">
                                        <option>Quimic1</option>
                                        <option>Infomasa</option>
                                    </select>     
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data garantia</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataGarantiaMaterialInsert" name="dataGarantiaMaterialInsert" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Observacions</label>
                                <div class=" col-md-10 col-sm-10" >
                                    <textarea rows="4" cols="50" class="form-control" id="observacionsMaterialInsert" name="observacionsMaterialInsert">
                                    </textarea>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <span id="btnAnadirMaterial" type="submit" class="btn btn-primary" data-action="save" role="button">Afegir</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modificarMaterial" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Modificar Material</h3>
                    </div>
                    <div>
                        <form id="formModal" method="post">
                            <input id="id" type="hidden"  name="id"/>
                            <div class="form-group col-md-12 col-sm-12 firstGroup">
                                <label class="col-md-2 col-sm-2">Nom</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="text"  id="nomMaterial"  name="nomMaterial">                                    
                                </div>
                                <label class="col-md-2 col-sm-2" >Descripció</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control col-md-4 col-sm-4" type="text" id="descripcioMaterial"  name="descripcioMaterial">                 
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Número de serie</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="numSerieMaterial" name="numSerieMaterial">                             
                                </div>
                                <label class="col-md-2 col-sm-2">Marca</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="marcaMaterial" name="marcaMaterial">           
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Model</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="modelMaterial" name="modelMaterial">                         
                                </div>
                                <label class="col-md-2 col-sm-2">Tipus</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="tipusMaterial" name="tipusMaterial">      </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Inventariable</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="checkbox" id="inventariableMaterial" name="inventariableMaterial">                  
                                </div>
                                <label class="col-md-2 col-sm-2">Identificador</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="identificadorMaterial" name="identificadorMaterial">
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data caducitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadMaterial" name="dataCadMaterial"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-md-2 col-sm-2">Valor per unitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="valorMaterial" name="valorMaterial">
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat inicial</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatIniMaterial" name="quantitatIniMaterial">        
                                </div>
                                <label class="col-md-2 col-sm-2">Quantitat actual</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatActMaterial" name="quantitatActMaterial">        
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat mínima</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatMinMaterial" name="quantitatMinMaterial">
                                </div>
                                <label class="col-md-2 col-sm-2">Proveidor</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="proveidorMaterial"  name="proveidorMaterial">
                                        <option>Quimic1</option>
                                        <option>Infomasa</option>
                                    </select>     
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data garantia</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataGarantiaMaterial" name="dataGarantiaMaterial" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Observacions</label>
                                <div class=" col-md-10 col-sm-10" >
                                    <textarea rows="4" cols="50" class="form-control" id="observacionsMaterial" name="observacionsMaterial">
                                    </textarea>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <span id="saveImage" type="submit" class="btn btn-primary" data-action="save" role="button">Modificar</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="consultarModal" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Consultar Material</h3>
                    </div>
                    <div>
                        <form id="formModal" method="post">
                            <div class="form-group col-md-12 col-sm-12 firstGroup">
                                <label class="col-md-2 col-sm-2">Nom</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="text"  id="nomMaterialConsulta"  name="nomMaterial" disabled>                                    
                                </div>
                                <label class="col-md-2 col-sm-2" >Descripció</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control col-md-4 col-sm-4" type="text" id="descripcioMaterialConsulta"  name="descripcioMaterial" disabled>                 
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Número de serie</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="numSerieMaterialConsulta" name="numSerieMaterial" disabled>                             
                                </div>
                                <label class="col-md-2 col-sm-2">Marca</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="marcaMaterialConsulta" name="marcaMaterial" disabled>           
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Model</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="modelMaterialConsulta" name="modelMaterial" disabled>                         
                                </div>
                                <label class="col-md-2 col-sm-2">Tipus</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="tipusMaterialConsulta" name="tipusMaterialConsulta" disabled>      </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Inventariable</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input class="form-control" type="checkbox" id="inventariableMaterialConsulta" name="inventariableMaterial" disabled>                  
                                </div>
                                <label class="col-md-2 col-sm-2">Identificador</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="identificadorMaterialConsulta" name="identificadorMaterial" disabled>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data caducitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadMaterialConsulta" name="dataCadMaterial" disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label class="col-md-2 col-sm-2">Valor per unitat</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="valorMaterialConsulta" name="valorMaterial" disabled>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat inicial</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatIniMaterialConsulta" name="quantitatIniMaterial" disabled>        
                                </div>
                                <label class="col-md-2 col-sm-2">Quantitat actual</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatActMaterialConsulta" name="quantitatActMaterial" disabled>        
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Quantitat mínima</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <input type="text" class="form-control" id="quantitatMinMaterialConsulta" name="quantitatMinMaterial" disabled>
                                </div>
                                <label class="col-md-2 col-sm-2">Proveidor</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <select type="text" class="form-control" id="proveidorMaterialConsulta"  name="proveidorMaterialConsulta" disabled>
                                        <option>Sense Informaci�</option>
                                    </select>     
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Data garantia</label>
                                <div class=" col-md-4 col-sm-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataGarantiaMaterialConsulta" name="dataGarantiaMaterial"  disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <label class="col-md-2 col-sm-2">Observacions</label>
                                <div class=" col-md-10 col-sm-10" >
                                    <textarea rows="4" cols="50" class="form-control" id="observacionsMaterialConsulta" name="observacionsMaterial" disabled>
                                    </textarea>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <div class="modal fade" id="eliminarMaterial" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Alerta!</h3>
                    </div>
                    <div class="modal-body">
                        <input id="idEliminar" type="hidden"  name="idEliminar"/>

                        <!-- content goes here -->
                        <span>Segur que vols eliminar aquesta informació?</span>
                        </br>
                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">No</button>
                        <button type="button" id="btnEliminarMaterial" class="btn  btn-primary"  role="button">Si</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmTancarSessio" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Segur que vols tancar la sessió?</h3>
                    </div>
                    <div class="modal-body">
                        <!-- content goes here -->
                        <span>Tingues en compte que si tanques la sessió, ja no podràs continuar navegant</span>  </br>     </br>                      
                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">No</button>
                        <button type="button" id="siTancaSessio" class="btn  btn-primary"  role="button">Si</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="canviaPassModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Canviar la contrasenya</h3>
                    </div>
                    <div>
                        <!-- content goes here -->
                        <div id="infoPass"></div>
                        <div class="form-group col-md-12 col-sm-12 firstGroup">
                            <label class="col-md-4 col-sm-4">Nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass1" name="newPass1">                             
                            </div>
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label class="col-md-4 col-sm-4">Confirmar nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass2" name="newPass2">           
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <span id="canviaContrasenya" type="submit" class="btn btn-primary" data-action="save" role="button">Canvia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--MODALES FIN -->    

        <!--HEADER-->
        <div class="banner">
            <div class="bienvenida">
                <?php
                echo 'Benvingut, ' . utf8_encode($_SESSION['nom']) . ' ' . utf8_encode($_SESSION['cognom']);
                if (isset($_SESSION['noHash'])) {
                    echo "<div class='alert alert-danger' role='alert'>CONTRASEÑA NO HASHEADA</div>";
                }
                ?>
            </div>
            <div>        
                <img src="img/giet.png"/>
            </div>      
        </div>
        <nav class="nav nav-tabs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="material.php"><span>Material</span></a></li>                        
                        <li class="altaUsuario"><a href="registro.php"><span>Usuaris</span></a></li>
                        <li><a href="contenidors.php"><span>Contenidors</span></a></li>
                        <li class="asignar"><a href="AsignarMaterial.php"><span>Assignar material</span></a></li>
                        <li class="asignar"><a href="AsignarContenedor.php"><span>Assignar Contenidors</span></a></li>
                    </ul>
                    <div class="pull-right">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> El meu compte<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a onclick="limpiaModalCTS()" href="#" data-toggle="modal" data-target="#canviaPassModal"><i class="icon-cog"></i>Canviar la contrasenya</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-toggle="modal" data-target="#confirmTancarSessio"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Tancar la sessió</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--FIN HEADER-->

        <!--SELECTS-->
        <div class="contenidoLabels col-md-12 col-sm-12">                           
            <div class="col-md-2 col-sm-2">Departament</div>            
            <div class="col-md-2 col-sm-2">Cicles</div>            
            <div class="col-md-2 col-sm-2">Moduls</div>            
            <div class="col-md-2 col-sm-2">UFs</div>            
        </div>
        <div class="contenidoSelects combo-group col-md-12 col-sm-12">                           
            <!--ComboBox de Departamento -->
            <div class="col-md-2 col-sm-2">                    
                <select id="departamento" onChange="cambioCiclo()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de Ciclos -->
            <div class="col-md-2 col-sm-2">   
                <select id="ciclo" onChange="cambioModulo()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de Modulos -->
            <div class="col-md-2 col-sm-2">   
                <select id="modulo" onChange="cambioUF()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de UFS -->
            <div class="col-md-2 col-sm-2">   
                <select id="UF" onChange="busquedaLista()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>

            <div class="col-md-2 col-sm-2">
                <button onClick="construirTabla()" id="btnBuscar" type="submit" class="btn btn-primary" disabled>Busca</button></span>
            </div>
        </div>

        <!--FIN SELECTS-->

        <!-- BOTON AÑADIR MATERIAL -->
        <div>
            <button id="anadirMaterial" class="btn-primary anadir col-md-12 " data-toggle="modal" data-target="#insertaMaterial" aria-label="Insertar">
                Afegir Material
            </button>
        </div>
        <!-- BOTON AÑADIR MATERIAL -->

        <!--TABLA-->
        <div id="tablaResultado" class="combo-group col-md-12 col-sm-12 contenidotabla" hidden>
            <div id="infoDiv"></div>
            <div id="divSearch" class="form-group pull-right" >
                <input  id="search" type="text" class="search form-control" placeholder="Cerca">
            </div>
            <span class="counter pull-right"></span>
            <table id="consulta" class="table table-hover table-striped table-bordered">
                <thead id="headConsulta">                   
                </thead>
                <tbody id="bodyConsulta">                    
                </tbody>
            </table>
        </div>   
        <!--FIN TABLA-->
    </body>
</html>