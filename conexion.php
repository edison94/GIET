<?php

if (isset($_POST['type'])) {
    session_start();
    @include 'recargaSesion.php';

    $roleid = $_SESSION['roleId'];
    $userId = $_SESSION['userId'];

    @$id = $_POST['id'];
    $type = $_POST['type'];

    $conn = new mysqli("localhost", $roleid, "12345", "inventari");

    if ($conn->connect_error) {
        echo '<option><p style="color: red; text-align: left">L\'usuari o la contrasenya no existeix.</p></option>';
    } else if ($type == 'comboProveedores') {
        $sql = 'CALL llistarProveidors()';

        $result = mysqli_query($conn, $sql);

        while ($row = $result->fetch_assoc()) {
            echo "<option name = '" . $row["ID"] . "'>" . utf8_encode($row["Nom empresa"]) . "</option>";
        }
    } else if ($type == 'comboTipusMaterial') {
        $sql = 'CALL llistarTipusMaterials()';

        $result = mysqli_query($conn, $sql);

        while ($row = $result->fetch_assoc()) {
            echo "<option name = '" . $row["ID"] . "'>" . utf8_encode($row["Nom"]) . "</option>";
        }
    } else if ($type == 'consultaPermisos') {
        echo $roleid;
    } else if ($type == 'departamentos') {

//$sql = 'SELECT * FROM Departaments';
        $sql = 'CALL selectDepartaments(' . $_SESSION['userId'] . ')';

        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 1) {
            echo "<option name='0'>Tots</option>";
        }
        if (mysqli_num_rows($result) < 1) {
            echo "<option>Sense resultats</option>";
        }

        while ($row = $result->fetch_assoc()) {
            echo "<option name = " . $row["ID"] . ">" . utf8_encode($row["Nom"]) . "</option>";
        }
        @mysqli_close($conn);
    } else if ($type == 'ciclos') {

        if ($id == 0) {
            $sql = 'CALL selectAllCicles(' . $_SESSION['userId'] . ')';
        } else {
            $sql = 'CALL selectCicles(' . $id . ',' . $_SESSION['userId'] . ')';
        }

        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 1) {
            echo "<option name='0'>Tots</option>";
        }
        if (mysqli_num_rows($result) < 1) {
            echo "<option>Sense resultats</option>";
        }

        while ($row = $result->fetch_assoc()) {
            echo "<option name = '" . $row["ID"] . "'>" . $row["Nomenclatura"] . " - " . utf8_encode($row["Nom"]) . "</option>";
        }

        @mysqli_close($conn);
    } else if ($type == 'modulos') {
        if ($id == 0) {
            $sql = 'CALL selectAllModuls(' . $_SESSION['userId'] . ')';
        } else {
//$sql = 'select * from Moduls where Cicle = ' . $id;
            $sql = 'CALL selectModuls(' . $id . ',' . $_SESSION['userId'] . ')';
        }
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 1) {
            echo "<option name='0'>Tots</option>";
        }
        if (mysqli_num_rows($result) < 1) {
            echo "<option>Sense resultats</option>";
        }

        while ($row = $result->fetch_assoc()) {
            echo "<option name = '" . $row["ID"] . "'>" . $row["Nomenclatura"] . " - " . utf8_encode($row["Nom"]) . "</option>";
        }

        @mysqli_close($conn);
    } else if ($type == 'UFS') {
        if ($id == 0) {
            $sql = 'CALL selectAllUFs(' . $_SESSION['userId'] . ')';
        } else {
//$sql = 'select * from UFs where Modul = ' . $id;
            $sql = 'CALL selectUFs(' . $id . ',' . $_SESSION['userId'] . ')';
        }
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 1) {
            echo "<option name='0'>Totes</option>";
        }
        if (mysqli_num_rows($result) < 1) {
            echo "<option>Sense resultats</option>";
        }

        while ($row = $result->fetch_assoc()) {
            echo "<option name = '" . $row["ID"] . "'>" . $row["Nomenclatura"] . " - " . utf8_encode($row["Nom"]) . "</option>";
        }

        @mysqli_close($conn);
    } else if ($type == 'tabla') {
        $departamento = $_POST['departamento'];
        $ciclo = $_POST['ciclo'];
        $modulo = $_POST['modulo'];
        $uf = $_POST['uf'];

        if ($departamento == 0) {
            $sql = 'CALL selectAllMaterial(' . $userId . ')';
        } else {
            if ($ciclo == 0) {
                $sql = 'CALL selectAllMaterial_departament(' . $departamento . ',' . $userId . ')';
            } else {
                if ($modulo == 0) {
                    $sql = 'CALL selectAllMaterial_cicle(' . $ciclo . ',' . $userId . ')';
                } else {
                    if ($uf == 0) {
                        $sql = 'CALL selectAllMaterial_modul(' . $modulo . ',' . $userId . ')';
                    } else {
                        $sql = 'CALL selectAllMaterial_uf(' . $uf . ',' . $userId . ')';
                    }
                }
            }
        }
		
        $result = mysqli_query($conn, $sql);

///************DESCOMENTAR EN CLASE***************
        while ($row = $result->fetch_assoc()) {
            echo '<tr><td>' . utf8_encode($row["Nom"]) . '</td><td>' . utf8_encode($row["Descripcio"]) . '</td><td>' . utf8_encode($row["Marca"]) . '</td><td>' . utf8_encode($row["Model"]) . '</td><td>' . utf8_encode($row["TipusMaterial"]) . '</td>' .
///************DESCOMENTAR EN CLASE***************
///************ELIMINAR EN CLASE***************
//        echo '<tr><td>PC</td><td>Ordenador Alumno J21</td><td>HP</td><td>SADK123S</td><td>Ordenador</td>'
///************ELIMINAR EN CLASE***************'

            '<td>
<a class="btn btn-primary pMod" data-toggle="modal" onclick="rellenaModal(' .
            $row["ID"] //DATOREAL 
//        1 //DATOFALSO
            . ')" data-target="#modificarMaterial" aria-label="Modifica">
  <i class="glyphicon glyphicon-edit" aria-hidden="true"></i>
</a>
<a data-toggle="modal" data-target="#eliminarMaterial" onClick="rellenaModalElimina(' .
            $row["ID"]
            . ')" class="btn btn-primary pEli" aria-label="Elimina">
  <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
</a>
<a data-toggle="modal" data-target="#consultarModal" onclick="rellenaModalConsulta(' .
            $row["ID"] //DATOREAL 
//        1 //DATOFALSO
            . ')"class="btn btn-primary pVer" aria-label="Consulta">
  <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
</a>
</td>
</tr>';
        }
///************ELIMINAR EN CLASE***************
//        echo '<tr><td>PC</td><td>Ordenador Alumno J21</td><td>HP</td><td>SADK123S</td><td>Ordenador</td>' .
///************ELIMINAR EN CLASE***************

        include 'ajax.php';
        if (auto_logout("start")) {
            session_unset();
            session_destroy();
            location("/error.php");
            exit;
        }
        @mysqli_close($conn);
    } else if ($type == 'permisosUsuarios') {
        $sql = 'CALL selectPermisos()';

        $result = mysqli_query($conn, $sql);

        while ($row = $result->fetch_assoc()) {
            echo '<option value="' . $row["ID"] . '">' . utf8_encode($row["Nom"]) . '</option>';
        }
    } else if ($type == 'ComentarioPermiso') {
        $id = $_POST['id'];

        $sql = 'CALL selectDescripcio(' . $id . ')';

        $result = mysqli_query($conn, $sql);

        while ($row = $result->fetch_assoc()) {
            echo utf8_encode($row["Comentaris"]);
        }
    } else if ($type == 'getModalInfo') {
        $id = $_POST['id'];

        $sql = 'CALL selectUnMaterial(' . $id . ')';

        $result = mysqli_query($conn, $sql);

        while ($Row = $result->fetch_assoc()) {
            $arr = array("id" => utf8_encode($Row["ID"]),
                'nom' => utf8_encode($Row["NomMaterial"]),
                'descripcio' => utf8_encode($Row["Descripcio"]),
                'marca' => utf8_encode($Row["Marca"]),
                'model' => utf8_encode($Row["Model"]),
                'tipusMaterial' => utf8_encode($Row["TipusMaterial"]),
                'inventariable' => utf8_encode($Row["inventariable"]),
                'identificador' => utf8_encode($Row["identificador"]),
                'dataCaducitat' => utf8_encode($Row["DataCaducitat"]),
                'valorPerUnitat' => utf8_encode($Row["valorPerUnitat"]),
                'quantitatInicial' => utf8_encode($Row["quantitatInicial"]),
                'quantitatActual' => utf8_encode($Row["quantitatActual"]),
                'quantitatMinima' => utf8_encode($Row["quantitatMinima"]),
                'proveidor' => utf8_encode($Row["Proveidor"]),
                'dataGarantia' => utf8_encode($Row["DataGarantia"]),
                'observacions' => utf8_encode($Row["Observacions"]),
                'numSerie' => utf8_encode($Row["numSerie"]),
            );
            echo implode('::', $arr);
        }
    } else if ($type == 'getModalDelete') {
        $id = $_POST['id'];

        $delete = 'CALL esborrarMaterial(' . $id . ',' . $userId . ')';
        $run = mysqli_query($conn, $delete);

        if (!$run) {
            echo "<div class='alert alert-danger' role='alert'>Material no esborrat</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Material esborrat</div>";
        }
    } else if ($type == 'insertarMaterial') {

        $insert = 'CALL  inserirMaterial( \'' .
                utf8_decode($_POST['nomMaterial']) . '\',\'' .
                utf8_decode($_POST['descripcioMaterial']) . '\',\'' .
                utf8_decode($_POST['numSerieMaterial']) . '\',\'' .
                utf8_decode($_POST['marcaMaterial']) . '\',\'' .
                utf8_decode($_POST['modelMaterial']) . '\',\'' .
                utf8_decode($_POST['tipusMaterial']) . '\',' .
                utf8_decode($_POST['inventariableMaterial']) . ',\'' .
                utf8_decode($_POST['identificadorMaterial']) . '\',\'' .
                utf8_decode($_POST['dataCadMaterial']) . '\',' .
                utf8_decode($_POST['valorMaterial']) . ',' .
                utf8_decode($_POST['quantitatIniMaterial']) . ',' .
                utf8_decode($_POST['quantitatActMaterial']) . ',' .
                utf8_decode($_POST['quantitatMinMaterial']) . ',' .
                utf8_decode($_POST['proveidorMaterial']) . ',\'' .
                utf8_decode($_POST['dataGarantiaMaterial']) . '\',\'' .
                utf8_decode($_POST['observacionsMaterial']) . '\',' .
                $userId .
                ')';

        $run = mysqli_query($conn, $insert);

        if (!$run) {
            echo "<div class='alert alert-danger' role='alert'>Material no insertado</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Material \"" . utf8_encode($_POST['nomMaterial']) . "\" afegit</div>";
        }
    } else if ($type == 'getModalInfoContenidor') {
        $id = $_POST['id'];

//        $sql = 'CALL selectUnContenidor('.$id.')';
        $sql = 'CALL selectUnContenidor(' . $id . ')';
        $result = mysqli_query($conn, $sql);


        $jsondata = array();
        while ($Row = $result->fetch_assoc()) {
            $arr = array(
                'tipus' => utf8_encode($Row["Tipus"]),
                'id' => utf8_encode($Row["ID"]),
                'ubicacio' => utf8_encode($Row["Ubicacio"]),
                'valor' => utf8_encode($Row["Valor"]),
                'datacaducitat' => utf8_encode($Row["DataCaducitat"]),
            );
            echo implode('::', $arr);
        }
    } else if ($type == 'modificarMaterial') {
        $update = 'CALL modificarMaterial( ' .
                utf8_decode($_POST['idMaterial']) . ',\'' .
                utf8_decode($_POST['nomMaterial']) . '\',\'' .
                utf8_decode($_POST['descripcioMaterial']) . '\',\'' .
                utf8_decode($_POST['numSerieMaterial']) . '\',\'' .
                utf8_decode($_POST['marcaMaterial']) . '\',\'' .
                utf8_decode($_POST['modelMaterial']) . '\',\'' .
                utf8_decode($_POST['tipusMaterial']) . '\',' .
                utf8_decode($_POST['inventariableMaterial']) . ',\'' .
                utf8_decode($_POST['dataCadMaterial']) . '\',' .
                utf8_decode($_POST['valorMaterial']) . ',' .
                utf8_decode($_POST['quantitatIniMaterial']) . ',' .
                utf8_decode($_POST['quantitatActMaterial']) . ',' .
                utf8_decode($_POST['quantitatMinMaterial']) . ',\'' .
                utf8_decode($_POST['proveidorMaterial']) . '\',\'' .
                utf8_decode($_POST['dataGarantiaMaterial']) . '\',\'' .
                utf8_decode($_POST['observacionsMaterial']) . '\',\'' .
                utf8_decode($_POST['identificadorMaterial']) .
                '\')';
        $role = mysqli_query($conn, $update);

        if (!$role) {
            echo "<div class='alert alert-danger' role='alert'>Material no modificat " . $update . mysqli_error($conn) . "</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Material modificat </div>";
        }
    } else if ($type == 'tablaMaterialEnContenidor') {

        $id = $_POST['id'];

        $sql = 'CALL selectAllMaterial_contenidor(' . $id . ')';
        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {
            echo '<tr><td>' . utf8_encode($row["Nom"]) . '</td><td>' . utf8_encode($row["Descripcio"]) . '</td><td>' . utf8_encode($row["Marca"]) . '</td><td>' . utf8_encode($row["Model"]) . '</td><td>' . utf8_encode($row["TipusMaterial"]) . '</td>' . '</tr>';
        }
//        include 'ajax.php';
//        if (auto_logout("start")) {
//            session_unset();
//            session_destroy();
//            location("/error.php");
//            exit;
//        }
//        @mysqli_close($conn);
    } else if ($type == 'tablaContenidor') {
        $departamento = $_POST['departamento'];
        $ciclo = $_POST['ciclo'];
        $modulo = $_POST['modulo'];
        $uf = $_POST['uf'];

        if ($departamento == 0) {
            $sql = 'CALL selectAllContenidors(' . $userId . ')';
        } else {
            if ($ciclo == 0) {
                $sql = 'CALL selectAllContenidors_departament(' . $departamento . ',' . $userId . ')';
            } else {
                if ($modulo == 0) {
                    $sql = 'CALL selectAllContenidors_cicle(' . $ciclo . ',' . $userId . ')';
                } else {
                    if ($uf == 0) {
                        $sql = 'CALL selectAllContenidors_modul(' . $modulo . ',' . $userId . ')';
                    } else {
                        $sql = 'CALL selectAllContenidors_uf(' . $uf . ',' . $userId . ')';
                    }
                }
            }
        }
        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {

            echo '<tr><td>' . utf8_encode($row["Nom"]) . '</td><td>' . utf8_encode($row["Ubicacio"]) . '</td><td>' . utf8_encode($row["Valor"]) . '</td><td>' . utf8_encode($row["DataCaducitat"]) . '</td>' . '
<td>
<a class="btn btn-primary" onClick="rellenaModalContenidor(' .
            $row["ID"]
            . ')" data-toggle="modal" data-target="#modificarContenidorModal" href="path/to/settings" aria-label="Delete">
  <i class="glyphicon glyphicon-edit" aria-hidden="true"></i>
</a>
<a onClick="eliminaContenidor(' .
            $row["ID"]
            . ')"  data-toggle="modal" data-target="#eliminarContenidor" class="btn btn-primary btnContenidor" href="path/to/settings" aria-label="Delete">
  <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
</a>
<a data-toggle="modal" data-target="#consultarContenidorModal" onClick="rellenaModalContenidorConsultar(' .
            $row["ID"]
            . ')" class="btn btn-primary" href="path/to/settings" aria-label="Delete">
  <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
</a>
</td>
</tr>
';
//echo "<option name = '" . $row["ID"] . "'>" . $row["Nomenclatura"] . " - " . utf8_encode($row["Nom"]) . "</option>";
        }
        include 'ajax.php';
        if (auto_logout("start")) {
            session_unset();
            session_destroy();
            location("/error.php");
            exit;
        }
        @mysqli_close($conn);
    } else if ($type == 'llenarComboTipus') {
        $sql = 'CALL selectTipusContenidors()';
        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {
            echo '<option name="' . $row["ID"] . '">' . $row["Nom"] . '</option>';
        }
    } else if ($type == 'insertContenidor') {

        $tipus = $_POST['tipus'];
        $identificador = $_POST['identificador'];
        $ubicacio = $_POST['ubicacio'];
        $valor = $_POST['valor'];
        $dataCaducitat = $_POST['dataCaducitat'];


        $sql = 'CALL inserirContenidor(\'' . $tipus . '\',\'' . $identificador . '\',\'' . $ubicacio . '\',' . $valor . ',\'' . $dataCaducitat . '\',' . $userId . ')';
//$result = mysqli_query($conn, $sql);

        $role = mysqli_query($conn, $sql);

        if (!$role) {
            echo "<div class='alert alert-danger' role='alert'>Error al afegir</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Afegit</div>";
        }
    } else if ($type == 'deleteContenidor') {

        $idContenidor = $_POST['id'];

        $sql = 'CALL esborrarContenidor(' . $idContenidor . ',' . $userId . ')';
        $role = mysqli_query($conn, $sql);

        if (!$role) {
            echo "<div class='alert alert-danger' role='alert'>Error al esborrar</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Esborrat</div>";
        }
    } else if ($type == 'modificarContenidor') {

        $sql = 'CALL modificarContenidor(' . $_POST['idContenidor'] . ',' . $_POST['tipus'] . ',\'' . $_POST['identificador'] . '\',\''
                . $_POST['ubicacio'] . '\',' . $_POST['valor'] . ',\'' . $_POST['dataCad'] . '\',' . $userId . ')';
        $role = mysqli_query($conn, $sql);


        if (!$role) {
            echo "<div class='alert alert-danger' role='alert'>Error al modificar</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Modificat</div>";
        }
    } else if ($type == 'materialLista') {

        $departamento = $_POST['departamento'];
        $ciclo = $_POST['ciclo'];
        $modulo = $_POST['modulo'];
        $uf = $_POST['uf'];

        if ($departamento == 0) {
            $sql = 'CALL selectAllMaterial(' . $userId . ')';
        } else {
            if ($ciclo == 0) {
                $sql = 'CALL selectAllMaterial_departament(' . $departamento . ',' . $userId . ')';
            } else {
                if ($modulo == 0) {
                    $sql = 'CALL selectAllMaterial_cicle(' . $ciclo . ',' . $userId . ')';
                } else {
                    if ($uf == 0) {
                        $sql = 'CALL selectAllMaterial_modul(' . $modulo . ',' . $userId . ')';
                    } else {
                        $sql = 'CALL selectAllMaterial_uf(' . $uf . ',' . $userId . ')';
                    }
                }
            }
        }
        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {
            echo '<tr><td>' . utf8_encode($row["Nom"]) . '</td><td>' . utf8_encode($row["Descripcio"]) . '</td><td>' . utf8_encode($row["Marca"]) . '</td><td>' . utf8_encode($row["Model"]) . '</td><td>' . utf8_encode($row["TipusMaterial"]) . '</td><td><input type="checkbox"  name="' . utf8_encode($row["ID"]) . '"/></td></tr>';
        }
    } else if ($type == 'contenidorLista') {

        $departamento = $_POST['departamento'];
        $ciclo = $_POST['ciclo'];
        $modulo = $_POST['modulo'];
        $uf = $_POST['uf'];

        if ($departamento == 0) {
            $sql = 'CALL selectAllContenidors(' . $userId . ')';
        } else {
            if ($ciclo == 0) {
                $sql = 'CALL selectAllContenidors_departament(' . $departamento . ',' . $userId . ')';
            } else {
                if ($modulo == 0) {
                    $sql = 'CALL selectAllContenidors_cicle(' . $ciclo . ',' . $userId . ')';
                } else {
                    if ($uf == 0) {
                        $sql = 'CALL selectAllContenidors_modul(' . $modulo . ',' . $userId . ')';
                    } else {
                        $sql = 'CALL selectAllContenidors_uf(' . $uf . ',' . $userId . ')';
                    }
                }
            }
        }
        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {
            echo '<tr><td>' . utf8_encode($row["Nom"]) . '</td><td>' . utf8_encode($row["Ubicacio"]) . '</td><td>' . utf8_encode($row["Valor"]) . '</td><td>' . utf8_encode($row["DataCaducitat"]) . '</td><td><input type="checkbox" name="' . utf8_encode($row["ID"]) . '"/></td></tr>';
        }
    } else if ($type == 'moureMaterial') {

        @$material = $_POST['materialArr'];
        @$contenidor = $_POST['contenidorArr'];

        if ($material != null) {
            foreach ($material as $idMaterial) {
                $sql = 'CALL treureMaterialDeContenidor(' . $idMaterial . ')';
                $result = mysqli_query($conn, $sql);
                if ($contenidor != null) {
                    foreach ($contenidor as $idContenidor) {
                        $sql = 'CALL assignarMaterialAContenidor(' . intval($idMaterial) . ',' . intval($idContenidor) . ')';
                        $result = mysqli_query($conn, $sql);
                    }
                }
                if ($result) {
                     echo "<div class='alert alert-success' role='alert'>Canvis desats</div>";
                }
            }
        } else {
            echo "<div class='alert alert-danger' role='alert'>No hi han hagut canvis</div>";
           
        }
    } else if ($type == 'cambiarPass') {

        $pass = hash('sha256', $_POST['pass']);

        $sql = 'CALL canviarContrasenya(\'' . $pass . '\',' . $userId . ')';
        $run = mysqli_query($conn, $sql);

        if (!$run) {
            echo "<div class='alert alert-danger' role='alert'>Contrasenya no modificada</div>";
        } else {
            echo "<div class='alert alert-success' role='alert'>Contrasenya modificada</div>";
        }
    } else if ($type == 'asignarContenidor') {

        @$ufArr = $_POST['ufArr'];
        @$contenidor = $_POST['contenidorArr'];

        if ($contenidor != null) {
            foreach (@$contenidor as $idContenidor) {
                $sql = 'CALL esborrarContenidorDeUF(' . $idContenidor . ')';
                @$result = mysqli_query($conn, $sql);
                if ($ufArr != null) {
                    foreach (@$ufArr as $idUf) {
                        $sql = 'CALL assignarContenidorAUF(' . intval($idUf) . ',' . intval($idContenidor) . ')';
                        $result = mysqli_query($conn, $sql);
                    }
                }
                if ($result) {
                    echo "<div class='alert alert-success' role='alert'>Canvis desats</div>";
                }
            }
        } else {
            echo "<div class='alert alert-danger' role='alert'>No hi han hagut canvis</div>";
        }
    } else if ($type == 'ufLista') {
        $idUf = $_POST['uf'];
        if ($idUf == 0) {
            $sql = 'CALL selectAllUFs(' . $_SESSION['userId'] . ')';
        } else {
//$sql = 'select * from UFs where Modul = ' . $id;
            $sql = 'select * from ufs where ID = ' . $idUf;
        }

        $result = mysqli_query($conn, $sql);
        while ($row = $result->fetch_assoc()) {
            echo '<tr><td>' . utf8_encode($row["Nomenclatura"]) . '</td><td>' . utf8_encode($row["Nom"]) . '</td><td><input type="checkbox" name="' . utf8_encode($row["ID"]) . '"/></td></tr>';
        }
    } else if ($type == 'registraUsuario') {

        $insert = 'CALL selectPermisos_ByMail( \'' . $_POST['emailRegis'] . '\')';
        $run = mysqli_query($conn, $insert);

        if (@mysqli_num_rows($run) == 1) {
            echo "<div class='alert alert-danger' role='alert'>L'usuari amb email " . $_POST['emailRegis'] . " ja existeix</div>";
        } else {
            $passReg = substr($_POST['emailRegis'], 0, (strrpos($_POST['emailRegis'], "@")));

            $insert = 'CALL inserirNouUsuari(' . $_POST['permisos'] . ',\'' . utf8_decode($_POST['nameRegis']) . '\',\'' . utf8_decode($_POST['lastNameRegis']) . '\',\'' . utf8_decode($_POST['emailRegis']) . '\',\'' . hash('sha256', $passReg) . '\',' . $userId . ')';
            mysqli_close($conn);
            $conn = new mysqli("localhost", $roleid, "12345", "inventari");
            $run = mysqli_query($conn, $insert);

            if (!$run) {
                echo "<div class='alert alert-danger' role='alert'>Usuario no insertado</div>";
            } else {
                echo "<div class='alert alert-success' role='alert'>Insertado</div>";
            }
        }
        include 'ajax.php';
        if (auto_logout("start")) {
            session_unset();
            session_destroy();
            location("/error.php");
            exit;
        }
        @mysqli_close($conn);
    }
}
?>