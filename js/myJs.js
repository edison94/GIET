/* =============================
 *          MATERIAL
 * =============================*/
$(function () {
    $(document).ready(function () {
        actPerm();
    });

    $(':checkbox').checkboxpicker({
        offLabel: 'No',
        onLabel: 'Si'
    });
    $(".datetimepicker1").datetimepicker({
        minView: 2,
        pickTime: false,
        locale: 'es',
        format: 'dd-mm-yyyy',
        autoclose: true
    });
});

function actPerm() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'consultaPermisos'
        },
        success: function (data) {
            switch (data) {
                case "1":
                    $(".pVer").show();
                    $(".pMod").hide();
                    $(".pEli").hide();
                    $(".altaUsuario").hide();
                    $(".asignar").hide();
                    break;
                case "2":
                    $(".pVer").show();
                    $(".pMod").hide();
                    $(".pEli").hide();
                    $(".altaUsuario").hide();
                    $(".asignar").show();
                    break;
                case "3":
                    $(".pVer").show();
                    $(".pMod").show();
                    $(".pEli").hide();
                    $(".altaUsuario").hide();
                    $(".asignar").show();
                    break;
                case "4":
                    $(".pVer").show();
                    $(".pMod").show();
                    $(".pEli").show();
                    $(".altaUsuario").hide();
                    $(".asignar").show();
                    break;
                case "5":
                    $(".pVer").show();
                    $(".pMod").show();
                    $(".pEli").show();
                    $(".altaUsuario").show();
                    $(".asignar").show();
                    break;
            }
        }
    });
}

function limpiaModalCTS() {
    $("#infoPass").html("");
}

function busquedaTabla(el) {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = $(el);
    filter = input.val().toUpperCase();
    table = $(el).parent().parent().find("tbody");
    tr = table.children("tr");

    for (i = 0; i < tr.length; i++) {
        contiene = false;
        tds = tr[i].getElementsByTagName("td")
        for (j = 0; j < tds.length; j++) {
            if (tds[j]) {
                if (tds[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    contiene = true;
                }
            }
        }
        if (contiene) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
    }
}

function comboProveedor() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'comboProveedores'
        },
        success: function (response) {
            document.getElementById("proveidorMaterial").innerHTML = response;
            document.getElementById("proveidorMaterialInsert").innerHTML = response;
            document.getElementById("proveidorMaterialConsulta").innerHTML = response;
        }
    });
}
function comboTipusMaterial() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'comboTipusMaterial'
        },
        success: function (response) {
            document.getElementById("tipusMaterial").innerHTML = response;
            document.getElementById("tipusMaterialInsert").innerHTML = response;
            document.getElementById("tipusMaterialConsulta").innerHTML = response;
        }
    });
}

function cambioDepartamento() {
    var val = 1;
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: val,
            type: 'departamentos'
        },
        success: function (response) {
            document.getElementById("departamento").innerHTML = response;
            cambioCiclo();
        }
    });
}

function cambioCiclo() {
    var val = document.getElementById("departamento").options[document.getElementById("departamento").selectedIndex];

    if ($(val).attr("name") == 0) {
        $("#ciclo").prop('disabled', true);
    } else {
        $("#ciclo").prop('disabled', false);
    }

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: $(val).attr("name"),
            type: 'ciclos'
        },
        success: function (response) {
            document.getElementById("ciclo").innerHTML = response;
            cambioModulo();

        }
    });
}

function cambioModulo() {
    var val = document.getElementById("ciclo").options[document.getElementById("ciclo").selectedIndex];

    if ($(val).attr("name") == 0) {
        $("#modulo").prop('disabled', true);
    } else {
        $("#modulo").prop('disabled', false);
    }

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: $(val).attr("name"),
            type: 'modulos'
        },
        success: function (response) {
            document.getElementById("modulo").innerHTML = response;
            cambioUF();
        }
    });
}


function cambioUF() {
    var val = document.getElementById("modulo").options[document.getElementById("modulo").selectedIndex];

    if ($(val).attr("name") == 0) {
        $("#UF").prop('disabled', true);
    } else {
        $("#UF").prop('disabled', false);
    }

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: $(val).attr("name"),
            type: 'UFS'
        },
        success: function (response) {
            document.getElementById("UF").innerHTML = response;
            busquedaLista();
            busquedaListaContenidor();
            busquedaListaMaterialVsContenidors();
            busquedaListaAsignarContenidor();
        }
    });

}

function busquedaLista(val) {
    var val = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];
    if ($(val).attr("name") >= 0) {
        $("#btnBuscar").prop('disabled', false);
    } else {
        $("#btnBuscar").prop('disabled', true);
    }
}

function construirTabla() {
    var departamento = document.getElementById("departamento").options[document.getElementById("departamento").selectedIndex];
    var ciclo = document.getElementById("ciclo").options[document.getElementById("ciclo").selectedIndex];
    var modulo = document.getElementById("modulo").options[document.getElementById("modulo").selectedIndex];
    var uf = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            departamento: $(departamento).attr("name"),
            ciclo: $(ciclo).attr("name"),
            modulo: $(modulo).attr("name"),
            uf: $(uf).attr("name"),
            type: "tabla"


        },
        success: function (response) {
            document.getElementById("headConsulta").innerHTML = "<tr><th class=\"col-md-2 col-xs-2\">Nom</th><th class=\"col-md-2 col-xs-2\">Descripcio</th><th class=\"col-md-2 col-xs-2\">Marca</th><th class=\"col-md-2 col-xs-2\">Model</th><th class=\"col-md-2 col-xs-2\">Tipus</th><th></th></tr>";
            document.getElementById("tablaResultado").hidden = false;
            if (response != "") {
                document.getElementById("bodyConsulta").innerHTML = response;
            } else {
                document.getElementById("bodyConsulta").innerHTML = "<tr><td colspan=\"6\">No hay resultados</td></tr>";
            }
            actPerm();
        }
    });

    //document.getElementById("consulta").innerHTML = contenido;
}

function rellenaModal(id) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'getModalInfo',
            id: id
        },
        success: function (data) {
            var res = data.split("::");
            $("#id").val(res[0]);
            $("#nomMaterial").val(res[1]);
            $("#descripcioMaterial").val(res[2]);
            $("#numSerieMaterial").val(res[16]);
            $("#marcaMaterial").val(res[3]);
            $("#modelMaterial").val(res[4]);
            $("#tipusMaterial").val(res[5]);
            if (res[6] == 1) {
                $("#inventariableMaterial").prop("checked", true);
            } else {
                $("#inventariableMaterial").prop("checked", false);
            }
            $("#identificadorMaterial").val(res[7]);
            $("#dataCadMaterial").val(res[8]);
            $("#valorMaterial").val(res[9]);
            $("#quantitatIniMaterial").val(res[10]);
            $("#quantitatActMaterial").val(res[11]);
            $("#quantitatMinMaterial").val(res[12]);
            $("#proveidorMaterial").val(res[13]);
            $("#dataGarantiaMaterial").val(res[14]);
            $("#observacionsMaterial").val(res[15]);
        }
    });
}
function rellenaModalConsulta(id) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'getModalInfo',
            id: id
        },
        success: function (data) {
            var res = data.split("::");
            $("#nomMaterialConsulta").val(res[1]);
            $("#descripcioMaterialConsulta").val(res[2]);
            $("#numSerieMaterialConsulta").val(res[16]);
            $("#marcaMaterialConsulta").val(res[3]);
            $("#modelMaterialConsulta").val(res[4]);
            $("#tipusMaterialConsulta").val(res[5]);
            if (res[6] == 1) {
                $("#inventariableMaterialConsulta").prop("checked", true);
            } else {
                $("#inventariableMaterialConsulta").prop("checked", false);
            }
            $("#identificadorMaterialConsulta").val(res[7]);
            $("#dataCadMaterialConsulta").val(res[8]);
            $("#valorMaterialConsulta").val(res[9]);
            $("#quantitatIniMaterialConsulta").val(res[10]);
            $("#quantitatActMaterialConsulta").val(res[11]);
            $("#quantitatMinMaterialConsulta").val(res[12]);
            $("#proveidorMaterialConsulta").val(res[13]);
            $("#dataGarantiaMaterialConsulta").val(res[14]);
            $("#observacionsMaterialConsulta").val(res[15]);
        }
    });
}
function rellenaModalElimina(id) {
    $("#idEliminar").val(id);
}
;
function eliminaMaterial() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'getModalDelete',
            id: $("#idEliminar").val()
        },
        success: function (data) {
            $("#eliminarMaterial").modal('toggle');
            $("#infoDiv").html(data);
            construirTabla();
        }
    });
}
;

function modificaMaterial() {
    if ($('#valorMaterial').val() == "") {
        $('#valorMaterial').val(0);
    }
    if ($('#quantitatIniMaterial').val() == "") {
        $('#quantitatIniMaterial').val(0);
    }
    if ($('#quantitatActMaterial').val() == "") {
        $('#quantitatActMaterial').val(0);
    }
    if ($('#quantitatMinMaterial').val() == "") {
        $('#quantitatMinMaterial').val(0);
    }

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            idMaterial: $('#id').val(),
            nomMaterial: $('#nomMaterial').val(),
            descripcioMaterial: $('#descripcioMaterial').val(),
            numSerieMaterial: $('#numSerieMaterial').val(),
            marcaMaterial: $('#marcaMaterial').val(),
            modelMaterial: $('#modelMaterial').val(),
            tipusMaterial: $('#tipusMaterial :selected').attr("name"),
            inventariableMaterial: $('#inventariableMaterial').prop("checked"),
            identificadorMaterial: $('#identificadorMaterial').val(),
            dataCadMaterial: $('#dataCadMaterial').val(),
            valorMaterial: $('#valorMaterial').val(),
            quantitatIniMaterial: $('#quantitatIniMaterial').val(),
            quantitatActMaterial: $('#quantitatActMaterial').val(),
            quantitatMinMaterial: $('#quantitatMinMaterial').val(),
            proveidorMaterial: $('#proveidorMaterial :selected').attr("name"),
            dataGarantiaMaterial: $('#dataGarantiaMaterial').val(),
            observacionsMaterial: $('#observacionsMaterial').val(),
            type: 'modificarMaterial'
        },
        success: function (data) {
            $("#modificarMaterial").modal('toggle');
            $("#infoDiv").html(data);
            construirTabla();
        }

    });
}

function insertaMaterial() {
    if ($('#valorMaterialInsert').val() == "") {
        $('#valorMaterialInsert').val(0);
    }
    if ($('#quantitatIniMaterialInsert').val() == "") {
        $('#quantitatIniMaterialInsert').val(0);
    }
    if ($('#quantitatActMaterialInsert').val() == "") {
        $('#quantitatActMaterialInsert').val(0);
    }
    if ($('#quantitatMinMaterialInsert').val() == "") {
        $('#quantitatMinMaterialInsert').val(0);
    }
    if ($('#dataCadMaterialInsert').val() == "") {
        $('#dataCadMaterialInsert').val(null);
    }
    if ($('#dataGarantiaMaterialInsert').val() == "") {
        $('#dataGarantiaMaterialInsert').val(null);
    }


    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            nomMaterial: $('#nomMaterialInsert').val(),
            descripcioMaterial: $('#descripcioMaterialInsert').val(),
            numSerieMaterial: $('#numSerieMaterialInsert').val(),
            marcaMaterial: $('#marcaMaterialInsert').val(),
            modelMaterial: $('#modelMaterialInsert').val(),
            tipusMaterial: $('#tipusMaterialInsert :selected').attr("name"),
            inventariableMaterial: $('#inventariableMaterialInsert').prop("checked"),
            identificadorMaterial: $('#identificadorMaterialInsert').val(),
            dataCadMaterial: $('#dataCadMaterialInsert').val(),
            valorMaterial: $('#valorMaterialInsert').val(),
            quantitatIniMaterial: $('#quantitatIniMaterialInsert').val(),
            quantitatActMaterial: $('#quantitatActMaterialInsert').val(),
            quantitatMinMaterial: $('#quantitatMinMaterialInsert').val(),
            proveidorMaterial: $("#proveidorMaterialInsert :selected").attr("name"),
            dataGarantiaMaterial: $('#dataGarantiaMaterialInsert').val(),
            observacionsMaterial: $('#observacionsMaterialInsert').val(),
            type: 'insertarMaterial'
        },
        success: function (data) {
            console.log(data);
            $("#insertaMaterial").modal('toggle');
            $("#infoDiv").html(data);
            construirTabla();

        }

    });
}
/* =============================
 *          USUARIOS
 * =============================*/

function comboPermisos() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'permisosUsuarios'
        },
        success: function (data) {

            document.getElementById("permisos").innerHTML = data;
            getComentarioXId();
        }
    });
}

function getComentarioXId() {
    var id = document.getElementById("permisos").options[document.getElementById("permisos").selectedIndex].value;
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'ComentarioPermiso',
            id: id
        },
        success: function (data) {
            var permiso = document.getElementById("permisos").options[document.getElementById("permisos").selectedIndex].text;
            document.getElementById("comentario").innerHTML = "<b>" + permiso + ":</b> " + data;
        }
    });
}

function registraUsuario() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            nameRegis: $('#nameRegis').val(),
            lastNameRegis: $('#lastNameRegis').val(),
            emailRegis: $('#emailRegis').val(),
            permisos: $('#permisos').val(),
            type: 'registraUsuario'
        },
        success: function (data) {
            $("#infoRegistro").html(data);

        }

    });
}

/* =============================
 *          CONTENIDORS
 * =============================*/

function construirTablaMaterialEnContenidorConsultar(id) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: id,
            type: "tablaMaterialEnContenidor"


        },
        success: function (response) {
            document.getElementById("headConsultaConsultar").innerHTML = "<tr><th class=\"col-md-2 col-xs-2\">Nom</th><th class=\"col-md-2 col-xs-2\">Descripcio</th><th class=\"col-md-2 col-xs-2\">Marca</th><th class=\"col-md-2 col-xs-2\">Model</th><th class=\"col-md-2 col-xs-2\">Tipus</th></tr>";
            document.getElementById("divSearchConsultar").hidden = false;
            if (response != "") {
                document.getElementById("bodyConsultaConsultar").innerHTML = response;
            } else {
                document.getElementById("bodyConsultaConsultar").innerHTML = "<tr><td colspan=\"5\">No hay resultados</td></tr>";
            }
        }
    });
}
function construirTablaMaterialEnContenidor(id) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: id,
            type: "tablaMaterialEnContenidor"
        },
        success: function (response) {
            document.getElementById("headConsulta").innerHTML = "<tr><th class=\"col-md-2 col-xs-2\">Nom</th><th class=\"col-md-2 col-xs-2\">Descripcio</th><th class=\"col-md-2 col-xs-2\">Marca</th><th class=\"col-md-2 col-xs-2\">Model</th><th class=\"col-md-2 col-xs-2\">Tipus</th></tr>";
            document.getElementById("divSearch").hidden = false;
            if (response != "") {
                document.getElementById("bodyConsulta").innerHTML = response;
            } else {
                document.getElementById("bodyConsulta").innerHTML = "<tr><td colspan=\"5\">No hay resultados</td></tr>";
            }
        }
    });
}
function construirTablaContenidor() {
    var departamento = document.getElementById("departamento").options[document.getElementById("departamento").selectedIndex];
    var ciclo = document.getElementById("ciclo").options[document.getElementById("ciclo").selectedIndex];
    var modulo = document.getElementById("modulo").options[document.getElementById("modulo").selectedIndex];
    var uf = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            departamento: $(departamento).attr("name"),
            ciclo: $(ciclo).attr("name"),
            modulo: $(modulo).attr("name"),
            uf: $(uf).attr("name"),
            type: "tablaContenidor"


        },
        success: function (response) {
            document.getElementById("headConsultaContenidor").innerHTML = "<tr><th class=\"col-md-2 col-sm-2\">Tipus</th><th class=\"col-md-2 col-sm-2\">Ubicacio</th><th class=\"col-md-2 col-sm-2\">Valor</th><th class=\"col-md-2 col-sm-2\">Data caducitat</th><th></th></tr>";
            document.getElementById("divSearchContenidor").hidden = false;
            if (response != "") {
                document.getElementById("bodyConsultaContenidor").innerHTML = response;
            } else {
                document.getElementById("bodyConsultaContenidor").innerHTML = "<tr><td colspan=\"5\">No hay resultados</td></tr>";
            }
        }
    });

    //document.getElementById("consulta").innerHTML = contenido;
}

function busquedaListaContenidor(val) {
    var val = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];
    if ($(val).attr("name") >= 0) {
        $("#btnBuscarAsignarMaterial").prop('disabled', false);
    } else {
        $("#btnBuscarContenidors").prop('disabled', true);
    }
}
function busquedaListaMaterialVsContenidors(val) {
    var val = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];
    if ($(val).attr("name") >= 0) {
        $("#btnBuscarContenidors").prop('disabled', false);
    } else {
        $("#btnBuscarContenidors").prop('disabled', true);
    }
}
function  busquedaListaAsignarContenidor() {
    var val = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];
    if ($(val).attr("name") >= 0) {
        $("#btnBuscarAsignarContenidor").prop('disabled', false);
    } else {
        $("#btnBuscarAsignarContenidor").prop('disabled', true);
    }
}


function rellenaModalContenidor(id) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'getModalInfoContenidor',
            id: id
        },
        success: function (data) {
            var res = data.split("::");
            $("#tipusContenidor").prop('selectedIndex', res[0] - 1);
            $("#identificadorContenidor").val(res[1]);
            $("#ubicacioContenidor").val(res[2]);
            $("#valorContenidor").val(res[3]);
            $("#dataCadContenidor").val(res[4]);
            $("#idModificar").val(id);
            construirTablaMaterialEnContenidor(id);
        }
    });
}
;
function rellenaModalContenidorConsultar(id) {

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'getModalInfoContenidor',
            id: id
        },
        success: function (data) {
            var res = data.split("::");
            $("#tipusContenidorConsultar").prop('selectedIndex', res[0] - 1);
            $("#identificadorContenidorConsultar").val(res[1]);
            $("#ubicacioContenidorConsultar").val(res[2]);
            $("#valorContenidorConsultar").val(res[3]);
            $("#dataCadContenidorConsultar").val(res[4]);
            construirTablaMaterialEnContenidorConsultar(id);
        }
    });
}
;

function modificaContenidor() {
    var tipus = document.getElementById("tipusContenidor").options[document.getElementById("tipusContenidor").selectedIndex];
    $.ajax({
        type: 'POST',
        url: 'conexion.php',
        data: {
            tipus: $(tipus).attr("name"),
            ubicacio: $('#ubicacioContenidor').val(),
            valor: $('#valorContenidor').val(),
            dataCad: $('#dataCadContenidor').val(),
            identificador: $('#identificadorContenidor').val(),
            idContenidor: $('#idModificar').val(),
            type: 'modificarContenidor'
        }, success: function (data) {
            $("#modificarContenidorModal").modal('toggle');
            $("#infoDiv").html(data);
            construirTablaContenidor();
        }
    });
}
function llenarComboTipus() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            type: 'llenarComboTipus'
        },
        success: function (response) {
            document.getElementById("tipusContenidorAfegir").innerHTML = response;
            document.getElementById("tipusContenidor").innerHTML = response;
            document.getElementById("tipusContenidorConsultar").innerHTML = response;
        }
    });
}

function insertContenidor() {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            tipus: $('#tipusContenidorAfegir').val(),
            identificador: $('#identificadorContenidorAfegir').val(),
            ubicacio: $('#ubicacioContenidorAfegir').val(),
            valor: $('#valorContenidorAfegir').val(),
            dataCaducitat: $('#dataCadContenidorAfegir').val(),
            type: 'insertContenidor'
        },
        success: function (data) {
            $("#afegirContenidorModal").modal('toggle');
            $("#infoDiv").append(data);
            construirTablaContenidor();
        }

    });
}
function eliminaContenidor(id) {
    $("#eliminaContenidor").val(id);
}
function eliminaContenidorConfirmado() {

    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            id: $('#eliminaContenidor').val(),
            type: 'deleteContenidor'
        },
        success: function (data) {
            $("#eliminarContenidor").modal('toggle');
            $("#infoDiv").html(data);
            construirTablaContenidor();

        }
    });
}

function construirTablasMaterialVsContenidors() {
    var departamento = document.getElementById("departamento").options[document.getElementById("departamento").selectedIndex];
    var ciclo = document.getElementById("ciclo").options[document.getElementById("ciclo").selectedIndex];
    var modulo = document.getElementById("modulo").options[document.getElementById("modulo").selectedIndex];
    var uf = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];

    listarMaterial(departamento, ciclo, modulo, uf);
    listarContenidors(departamento, ciclo, modulo, uf);


    //document.getElementById("consulta").innerHTML = contenido;
}
function construirTablasContenidorsVsUfs() {
    var departamento = document.getElementById("departamento").options[document.getElementById("departamento").selectedIndex];
    var ciclo = document.getElementById("ciclo").options[document.getElementById("ciclo").selectedIndex];
    var modulo = document.getElementById("modulo").options[document.getElementById("modulo").selectedIndex];
    var uf = document.getElementById("UF").options[document.getElementById("UF").selectedIndex];

    listarContenidorsvsUf(departamento, ciclo, modulo, uf);
    listarUfs(uf);
}

function listarMaterial(departamento, ciclo, modulo, uf) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            departamento: $(departamento).attr("name"),
            ciclo: $(ciclo).attr("name"),
            modulo: $(modulo).attr("name"),
            uf: $(uf).attr("name"),
            type: "materialLista"


        },
        success: function (data) {
            document.getElementById("listaMaterialHead").innerHTML = "<tr><th class=\"col-md-2 col-xs-2\">Nom</th><th class=\"col-md-2 col-xs-2\">Descripcio</th><th class=\"col-md-2 col-xs-2\">Marca</th><th class=\"col-md-2 col-xs-2\">Model</th><th class=\"col-md-2 col-xs-2\">Tipus</th><th>Moure</th></tr>";

            document.getElementById("listaMaterialBody").innerHTML = data;
            document.getElementById("divListaMaterial").hidden = false;
        }
    });
}




function listarContenidors(departamento, ciclo, modulo, uf) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            departamento: $(departamento).attr("name"),
            ciclo: $(ciclo).attr("name"),
            modulo: $(modulo).attr("name"),
            uf: $(uf).attr("name"),
            type: "contenidorLista"


        },
        success: function (data) {
            document.getElementById("listaContenidorHead").innerHTML = "<tr><th class=\"col-md-2 col-sm-2\">Tipus</th><th class=\"col-md-2 col-sm-2\">Ubicacio</th><th class=\"col-md-2 col-sm-2\">Valor</th><th class=\"col-md-2 col-sm-2\">Data caducitat</th><th>Contenidor destí</th></tr>";

            document.getElementById("listaContenidorBody").innerHTML = data;
            $(':checkbox').checkboxpicker({
                offLabel: 'No',
                onLabel: 'Si'
            });

            document.getElementById("divListaContenidor").hidden = false;
            document.getElementById("divMoure").hidden = false;

        }

    });
}
function listarContenidorsvsUf(departamento, ciclo, modulo, uf) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            departamento: $(departamento).attr("name"),
            ciclo: $(ciclo).attr("name"),
            modulo: $(modulo).attr("name"),
            uf: $(uf).attr("name"),
            type: "contenidorLista"


        }, success: function (data) {
            document.getElementById("listaContenidorvsUFHead").innerHTML = "<tr><th class=\"col-md-2 col-sm-2\">Tipus</th><th class=\"col-md-2 col-sm-2\">Ubicacio</th><th class=\"col-md-2 col-sm-2\">Valor</th><th class=\"col-md-2 col-sm-2\">Data caducitat</th><th>Contenidor destí</th></tr>";

            document.getElementById("listaContenidorvsUFBody").innerHTML = data;
            $(':checkbox').checkboxpicker({
                offLabel: 'No',
                onLabel: 'Si'
            });

            document.getElementById("divListaContenidorsvsUF").hidden = false;

        }
    });
}
function listarUfs(uf) {
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            uf: $(uf).attr("name"),
            type: "ufLista"


        }, success: function (data) {
            document.getElementById("listaUFHead").innerHTML = "<tr><th class=\"col-md-2 col-sm-2\">Nomenclatura</th><th class=\"col-md-2 col-sm-2\">Nom</th><th class=\"col-md-2 col-sm-2\">Seleccionada</th></tr>";

            document.getElementById("listaUFBody").innerHTML = data;
            $(':checkbox').checkboxpicker({
                offLabel: 'No',
                onLabel: 'Si'
            });

            document.getElementById("divListaUF").hidden = false;
            document.getElementById("divAssignar").hidden = false;

        }
    });
}

function moureMaterial() {
    var materialArray = [];
    $("#listaMaterialBody tr").each(function () {
        var checkTmp = $(this).find(".hidden");
        if (checkTmp.prop("checked")) {
            materialArray.push($(checkTmp).attr("name"));
        }
    });

    var contenidorArray = [];
    $("#listaContenidorBody tr").each(function () {
        var checkTmp = $(this).find(".hidden");
        if (checkTmp.prop("checked")) {
            contenidorArray.push($(checkTmp).attr("name"));
        }
    });
    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            materialArr: materialArray,
            contenidorArr: contenidorArray,
            type: "moureMaterial"
        },
        success: function (data) {
            $("#infoDiv").html(data);
        }
    });


}
function asignarContenidor() {
    var contenidorArray = [];
    $("#listaContenidorvsUFBody tr").each(function () {
        var checkTmp = $(this).find(".hidden");
        if (checkTmp.prop("checked")) {
            contenidorArray.push($(checkTmp).attr("name"));
        }
    });
    var ufArray = [];
    $("#listaUFBody tr").each(function () {
        var checkTmp = $(this).find(".hidden");
        if (checkTmp.prop("checked")) {
            ufArray.push($(checkTmp).attr("name"));
        }
    });


    $.ajax({
        type: 'post',
        url: 'conexion.php',
        data: {
            ufArr: ufArray,
            contenidorArr: contenidorArray,
            type: "asignarContenidor"
        },
        success: function (data) {
            $("#infoDiv").html(data);
        }
    });


}
function cambiarContrasena() {
    var novaContrasenya = $("#newPass1").val();
    if (novaContrasenya == $("#newPass2").val()) {
        $.ajax({
            type: 'post',
            url: 'conexion.php',
            data: {
                pass: novaContrasenya,
                type: "cambiarPass"
            },
            success: function (data) {
                $("#infoPass").html(data);
            }
        });
    } else {
        $("#infoPass").html("<div class='alert alert-danger' role='alert'>Les contrasenyes introduides no coicideixen</div>");
    }

}
