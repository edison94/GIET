<!DOCTYPE html>
<head>	
    <title>GIET - Accés denegat</title>
    <link rel="shortcut icon" href="/proyectoFinal/favicon.png" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".btn").click(function () {
                window.location.href = "myPage.php";
            });
        });
    </script>
</head>
<body>		
    <div class="main">
        <div class="container">
            <div id="divError" class="jumbotron gris">
                <h1>Accés prohibit <small><font face="Tahoma" color="red">Error 403</font></small></h1>
                <br />
                <p>Accés denegat a la pàgina solicitada.<b> Contacti amb el seu administrador web per més informació.</b> </p>
                <p>Torna a la pagina de identificació per iniciar la sessió o simplement prem el següent botó:</p>
                <a class="btn btn-primary"><i class="icon-home icon-white"></i> Inicia la sessió</a>
            </div>
        </div>
    </div>
</body>
