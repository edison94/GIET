<?php
if (auto_logout("start")) {
    session_unset();
    session_destroy();
    header('Location: error.php');              
    exit;
}
function auto_logout($field) {
    $t = time();
    $t0 = $_SESSION[$field];
    $diff = $t - $t0;

    if ($diff > 60 * 15 || !isset($t0)) {       
        return true;
    } else {
        $_SESSION[$field] = time();
    }
}
?>
