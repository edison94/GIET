<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");

if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>GIET - Registra Usuari</title>
        <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
        <link href="css/estilo.css" rel="stylesheet" type="text/css">
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" ></script>
        <script src="js/myJs.js"></script>      
        <script src="bootstrap/dist/js/bootstrap-checkbox.min.js" defer></script>        
        <script type="text/javascript" src="bootstrap/moment/moment.js"></script>
        <script type="text/javascript" src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/transition.js"></script>
        <script type="text/javascript" src="bootstrap/js/collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script src="js/myJs.js"></script>  
        <script type="text/javascript">
            $(document).ready(function () {
                //CARGAR COMBO PERMISOS
                comboPermisos();

                //PREPARA EVENTO DESCRIPCIONES PERMISOS
                $("#permisos").change(function () {
                    getComentarioXId();
                });

                $("#registra").click(function () {
                    registraUsuario();
                });
                $("#siTancaSessio").click(function () {
                    window.location.href = "tancaSessio.php";
                });
                $("#canviaContrasenya").click(function () {
                    cambiarContrasena();
                });


            });
        </script>
    </head>
    <body>    
        <div class="modal fade" id="confirmTancarSessio" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Segur que vols tancar la sessió?</h3>
                    </div>
                    <div class="modal-body">
                        <!-- content goes here -->
                        <span>Tingues en compte que si tanques la sessió, ja no podràs continuar navegant</span>  </br>     </br>                      
                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">No</button>
                        <button type="button" id="siTancaSessio" class="btn  btn-primary"  role="button">Si</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="canviaPassModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Canviar la contrasenya</h3>
                    </div>
                    <div>
                        <!-- content goes here -->
                        <div id="infoPass"></div>
                        <div class="form-group col-md-12 col-sm-12 firstGroup">
                            <label class="col-md-4 col-sm-4">Nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass1" name="newPass1">                             
                            </div>
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label class="col-md-4 col-sm-4">Confirmar nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass2" name="newPass2">           
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <span id="canviaContrasenya" type="submit" class="btn btn-primary" data-action="save" role="button">Canvia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--MODALES FIN -->    

        <!--HEADER-->
        <div class="banner">
            <div class="bienvenida">
                <?php
                echo 'Benvingut, ' . utf8_encode($_SESSION['nom']) . ' ' . utf8_encode($_SESSION['cognom']);
                if (isset($_SESSION['noHash'])) {
                    echo "<div class='alert alert-danger' role='alert'>CONTRASEÑA NO HASHEADA</div>";
                }
                ?>
            </div>
            <div>        
                <img src="img/giet.png"/>
            </div>      
        </div>
        <nav class="nav nav-tabs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="material.php"><span>Material</span></a></li>                        
                        <li class="altaUsuario active"><a href="registro.php"><span>Usuaris</span></a></li>
                        <li><a href="contenidors.php"><span>Contenidors</span></a></li>
                        <li class="asignar"><a href="AsignarMaterial.php"><span>Assignar material</span></a></li>
                        <li class="asignar"><a href="AsignarContenedor.php"><span>Assignar Contenidors</span></a></li>
                    </ul>
                    <div class="pull-right">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> El meu compte<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a onclick="limpiaModalCTS()" href="#" data-toggle="modal" data-target="#canviaPassModal"><i class="icon-cog"></i>Canviar la contrasenya</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-toggle="modal" data-target="#confirmTancarSessio"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Tancar la sessió</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--FIN HEADER-->

        <div class="contenido col-md-12">   
            <div class="col-md-12">
                <div id="infoRegistro"></div>
                <div class="form-group">
                    <label>Nom</label>
                    <input class="form-control col-md-9" type="text" name="userName" id="nameRegis" required>                            
                </div>
                <div class="form-group">
                    <label>Cognoms</label>
                    <input class="form-control"type="text" name="lastNameRegis" id="lastNameRegis" required>                         
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control"type="text" name="emailRegis" id="emailRegis" required>                          
                </div>
                <div class="form-group">
                    <label>Permisos</label>
                    <select id="permisos" class="form-control" name="perm" class="col-md-12">
                    </select>                
                </div>
                <div class="form-group">
                    <div id="comentario" class="alert alert-warning" role="alert">Insertado</div>
                </div>

                <div>
                    <button id="registra" name="registra" class="btn btn-primary">Registrar usuari</button>
                </div>
            </div>
        </div>
    </body>
</html>
