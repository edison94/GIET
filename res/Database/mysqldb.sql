-- Nomenclatura FK:
--	FK_origintable_field_pktable_pkfield


USE inventari;

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS Comandes;
DROP TABLE IF EXISTS Proveidors;
DROP TABLE IF EXISTS CodisPostals;
DROP TABLE IF EXISTS Poblacions;
DROP TABLE IF EXISTS Provincies;
DROP TABLE IF EXISTS Material_vs_Contenidors;
DROP TABLE IF EXISTS Contenidors_vs_UFs;
DROP TABLE IF EXISTS Material;
DROP TABLE IF EXISTS TipusMaterial;
DROP TABLE IF EXISTS Contenidor;
DROP TABLE IF EXISTS TipusContenidor;
DROP TABLE IF EXISTS UFs;
DROP TABLE IF EXISTS Moduls;
DROP TABLE IF EXISTS Cicles;
DROP TABLE IF EXISTS Usuaris;
DROP TABLE IF EXISTS Departaments;
DROP TABLE IF EXISTS Permisos;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE Permisos (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Nom VARCHAR(30),
	Comentaris VARCHAR(100)
);

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE Usuaris (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Permisos TINYINT UNSIGNED,
	Nom VARCHAR(15),
	Cognoms VARCHAR(30),
	Email VARCHAR(50),
	Password VARCHAR(100),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_usuaris_permisos_permisos_id FOREIGN KEY (Permisos) REFERENCES Permisos(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_usuaris_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_usuaris_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);


CREATE TABLE Departaments (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	CapDepartament SMALLINT UNSIGNED,
	Nom VARCHAR(30),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_departaments_capdepartament_usuaris_id FOREIGN KEY (CapDepartament) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_departaments_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_departaments_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE Cicles (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Departament TINYINT UNSIGNED,
	Nomenclatura VARCHAR(4),
	Nom VARCHAR(50),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_cicles_departament_departaments_id FOREIGN KEY (Departament) REFERENCES Departaments(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_cicles_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_cicles_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

CREATE TABLE Moduls (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Cicle TINYINT UNSIGNED,
	Nomenclatura VARCHAR(5),
	Nom VARCHAR(50),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_moduls_cicle_cicles_id FOREIGN KEY (Cicle) REFERENCES Cicles(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_moduls_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_moduls_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

CREATE TABLE UFs (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Professor SMALLINT UNSIGNED,
	Modul SMALLINT UNSIGNED,
	Nomenclatura VARCHAR(4),
	Nom VARCHAR(50),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_ufs_professor_usuaris_id FOREIGN KEY (Professor) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_ufs_modul_moduls_id FOREIGN KEY (Modul) REFERENCES Moduls(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_ufs_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_ufs_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

CREATE TABLE TipusContenidor (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Nom VARCHAR(30)
);

CREATE TABLE Contenidor (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Tipus TINYINT UNSIGNED,
	Identificador VARCHAR(30),
	Ubicacio VARCHAR(40),
	Valor DOUBLE(5,2),
	DataAlta DATE,
	DataCaducitat DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_contenidor_tipus_tipuscontenidor_id FOREIGN KEY (Tipus) REFERENCES TipusContenidor(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_contenidor_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,	
	CONSTRAINT FK_contenidor_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

CREATE TABLE TipusMaterial (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,	
	Nom VARCHAR(30),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,
	
	CONSTRAINT FK_tipusmaterial_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,	
	CONSTRAINT FK_tipusmaterial_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE	
);

CREATE TABLE Material (
	ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Nom VARCHAR(50),
	Descripcio VARCHAR(100),
	numSerie VARCHAR(30),
	Marca VARCHAR(30),
	Model VARCHAR(20),
	tipusMaterial TINYINT UNSIGNED,
	inventariable BOOLEAN,
	identificador VARCHAR(50),
	dataCaducitat DATE,
	valorPerUnitat FLOAT,
	quantitatInicial INT,
	quantitatActual INT,
	quantitatMinima INT,
	proveidor SMALLINT UNSIGNED,
	DataGarantia DATE,
	Observacions VARCHAR(100),
	dataAlta DATE,
	dataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,
	
	CONSTRAINT FK_material_tipusmaterial_tipusmaterial_id FOREIGN KEY (tipusMaterial) REFERENCES TipusMaterial(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_material_proveidor_proveidors_id FOREIGN KEY (proveidor) REFERENCES Proveidors(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_material_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,	
	CONSTRAINT FK_material_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

CREATE TABLE Contenidors_vs_UFs (
	UF SMALLINT UNSIGNED,
	Contenidor SMALLINT UNSIGNED,
	
	CONSTRAINT FK_contenidorvsufs_uf_ufs_id FOREIGN KEY (UF) REFERENCES UFs(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_contenidorvsufs_contenidor_contenidor_id FOREIGN KEY (Contenidor) REFERENCES Contenidor(ID) ON UPDATE CASCADE,
	PRIMARY KEY(UF,Contenidor)
);

CREATE TABLE Material_vs_Contenidors (
	Material INT UNSIGNED,
	Contenidor SMALLINT UNSIGNED,
	
	CONSTRAINT FK_materialvscontenidors_material_materials_id FOREIGN KEY (Material) REFERENCES Material(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_materialvscontenidors_contenidor_contenidor_id FOREIGN KEY (Contenidor) REFERENCES Contenidor(ID) ON UPDATE CASCADE,
	PRIMARY KEY (Contenidor,Material)
);

CREATE TABLE Provincies (
	ID TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Nom VARCHAR(10)
);

CREATE TABLE Poblacions (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Provincia TINYINT UNSIGNED,
	Nom VARCHAR(50),
	
	CONSTRAINT FK_poblacions_provincia_provincies_id FOREIGN KEY (Provincia) REFERENCES Provincies(ID) ON UPDATE CASCADE
);

CREATE TABLE CodisPostals (
	ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Poblacio SMALLINT UNSIGNED,

	CONSTRAINT FK_codispostals_poblacio_poblacions_id FOREIGN KEY (Poblacio) REFERENCES Poblacions(ID) ON UPDATE CASCADE
);

CREATE TABLE Proveidors (
	ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Nom VARCHAR(30),
	Descripcio VARCHAR(100),
	Direccio VARCHAR(100),
	CodiPostal INT UNSIGNED,
	Observacions VARCHAR(100),
	DataAlta DATE,
	DataBaixa DATE,
	UsuariDonaAlta SMALLINT UNSIGNED,
	UsuariDonaBaixa SMALLINT UNSIGNED,

	CONSTRAINT FK_proveidors_codipostal_codispostals_id FOREIGN KEY (CodiPostal) REFERENCES CodisPostals(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_proveidors_usuaridonaalta_usuaris_id FOREIGN KEY (UsuariDonaAlta) REFERENCES Usuaris(ID) ON UPDATE CASCADE,	
	CONSTRAINT FK_proveidors_usuaridonabaixa_usuaris_id FOREIGN KEY (UsuariDonaBaixa) REFERENCES Usuaris(ID) ON UPDATE CASCADE	
);

CREATE TABLE Comandes (
	ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	Material INT UNSIGNED,
	Quantitat SMALLINT UNSIGNED,
	DataComanda DATE,
	Comprador SMALLINT UNSIGNED,

	CONSTRAINT FK_comandes_material_material_id FOREIGN KEY (Material) REFERENCES Material(ID) ON UPDATE CASCADE,
	CONSTRAINT FK_comandes_comprador_usuaris_id FOREIGN KEY (Comprador) REFERENCES Usuaris(ID) ON UPDATE CASCADE
);

