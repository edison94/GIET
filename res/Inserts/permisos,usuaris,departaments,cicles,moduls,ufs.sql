USE inventari;


INSERT INTO Usuaris VALUES (1,5,'Direcció','Director','director@correu.escoladeltreball.org','1234',CURRENT_DATE,null,null,null);

INSERT INTO Usuaris VALUES (3,2,'Genís','Martínez','gmartinez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (4,1,'Ramón','Bruballa','rbruballa@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (5,1,'Carlos','Val','cval@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (6,2,'Jordi','Andúgar','jandugar@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (7,1,'Pep','Méndez','jmendez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (8,1,'Marc','Domingo','mdomingo@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (9,1,'Mónica','Martínez','mmartinez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (2,4,'Administració','1','administracio1@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);



INSERT INTO Departaments VALUES (1,3,'Informàtica',CURRENT_DATE,null,2,null);

INSERT INTO Departaments VALUES (2,6,'Química',CURRENT_DATE,null,2,null);


INSERT INTO Cicles VALUES (1,1,'DAM','Desenvolupament d''Aplicacions Multiplataforma');

INSERT INTO Cicles VALUES (2,1,'DAW','Desenvolupament d''Aplicacions Web');

INSERT INTO Cicles VALUES (3,2,'RQA','Reaccions Químiques Avançades');

INSERT INTO Cicles VALUES (4,2,'QOA','Química Orgànica Avançada');


INSERT INTO Moduls VALUES (1,1,'M01','Sistemes Informàtics');

INSERT INTO Moduls VALUES (2,1,'M03','Programació');

INSERT INTO Moduls VALUES (3,2,'M02','Bases de Dades');

INSERT INTO Moduls VALUES (4,2,'M04','Llenguatge de Marques');

INSERT INTO Moduls VALUES (5,3,'M01','Bombes caseras');

INSERT INTO Moduls VALUES (6,3,'M02','Cómo hacer meta');

INSERT INTO Moduls VALUES (7,4,'M01','La Marihuana i els seus efectes');

INSERT INTO Moduls VALUES (8,4,'M05','Cómo descomponer un cadáver');


INSERT INTO UFs VALUES (1,3,1,'UF1','Introducció als sistemes operatius',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (2,3,1,'UF2','Creació d''scripts',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (3,3,2,'UF1','Introducció a Java',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (4,4,2,'UF2','Herència i polimorfisme',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (5,3,3,'UF1','Disseny de bases de dades',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (6,4,3,'UF2','Queries',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (7,3,4,'UF1','Introducció al XML',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (8,3,4,'UF2','Introducció a Javascript',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (9,5,5,'UF1','Comprar materiales sin que te detecte el FBI',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (10,5,5,'UF2','Cómo no morir en el intento',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (11,5,6,'UF1','Dónde hacer tu pequeña cocina',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (12,6,6,'UF2','Sé Heisenberg',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (13,6,7,'UF1','Diferenciar entre orégano y maría',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (14,7,7,'UF2','Aliada o enemiga?',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (15,8,8,'UF1','Paso 1: Encontrar un cuerpo',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (16,9,8,'UF2','Paso 2: Hacer hamburguesas',CURRENT_DATE,null,2,null);
