USE inventari;

INSERT INTO Permisos VALUES (1, 'Profesor', 'Solo puede seleccionar cosas de sus UFs');

INSERT INTO Permisos VALUES (2, 'Jefe de departamento', 'Puede consultar todo el material de su departamento e insertar cosas en la base de datos');

INSERT INTO Permisos VALUES (4, 'Administración', 'Puede hacer consultas de todo el material, dar de alta y baja proveedores y material');

INSERT INTO Permisos VALUES (5, 'Dirección', 'Tiene permisos para hacer todo en la base de datos, menos borrar.');
