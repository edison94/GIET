USE inventari;

INSERT INTO Usuaris VALUES (1,5,'Direcció','Director','director@correu.escoladeltreball.org','1234',CURRENT_DATE,null,null,null);

INSERT INTO Usuaris VALUES (3,2,'Genís','Martínez','gmartinez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (4,1,'Ramón','Bruballa','rbruballa@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (5,1,'Carlos','Val','cval@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (6,2,'Jordi','Andúgar','jandugar@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (7,1,'Pep','Méndez','jmendez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (8,1,'Marc','Domingo','mdomingo@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (9,1,'Mónica','Martínez','mmartinez@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);

INSERT INTO Usuaris VALUES (2,4,'Administració','1','administracio1@correu.escoladeltreball.org','1234',CURRENT_DATE,null,1,null);
