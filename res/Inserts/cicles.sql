USE inventari;

INSERT INTO Cicles VALUES (1,1,'DAM','Desenvolupament d''Aplicacions Multiplataforma',CURRENT_DATE,null,1,null);

INSERT INTO Cicles VALUES (2,1,'DAW','Desenvolupament d''Aplicacions Web',CURRENT_DATE,null,1,null);

INSERT INTO Cicles VALUES (3,2,'RQA','Reaccions Químiques Avançades',CURRENT_DATE,null,1,null);

INSERT INTO Cicles VALUES (4,2,'QOA','Química Orgànica Avançada',CURRENT_DATE,null,1,null);

