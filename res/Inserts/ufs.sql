USE inventari;

INSERT INTO UFs VALUES (1,3,1,'UF1','Introducció als sistemes operatius',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (2,3,1,'UF2','Creació d''scripts',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (3,3,2,'UF1','Introducció a Java',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (4,4,2,'UF2','Herència i polimorfisme',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (5,3,3,'UF1','Disseny de bases de dades',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (6,4,3,'UF2','Queries',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (7,3,4,'UF1','Introducció al XML',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (8,3,4,'UF2','Introducció a Javascript',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (9,5,5,'UF1','Comprar materiales sin que te detecte el FBI',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (10,5,5,'UF2','Cómo no morir en el intento',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (11,5,6,'UF1','Dónde hacer tu pequeña cocina',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (12,6,6,'UF2','Sé Heisenberg',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (13,6,7,'UF1','Diferenciar entre orégano y maría',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (14,7,7,'UF2','Aliada o enemiga?',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (15,8,8,'UF1','Paso 1: Encontrar un cuerpo',CURRENT_DATE,null,2,null);

INSERT INTO UFs VALUES (16,9,8,'UF2','Paso 2: Hacer hamburguesas',CURRENT_DATE,null,2,null);
