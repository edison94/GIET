USE inventari;

DROP PROCEDURE IF EXISTS inserirProveidor;
DROP PROCEDURE IF EXISTS esborrarProveidor;
DROP PROCEDURE IF EXISTS modificarProveidor;
DROP PROCEDURE IF EXISTS llistarProveidors;

DELIMITER //

CREATE PROCEDURE inserirProveidor(IN Nom VARCHAR(30), IN Descripcio VARCHAR(100), IN Direccio VARCHAR(100), IN CodiPostal INTEGER(10) UNSIGNED, IN Observacions VARCHAR(100),IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Proveidors
VALUES
	(LAST_INSERT_ID(),
	Nom,
	Descripcio,
	Direccio,
	CodiPostal,
	Observacions,
	CURRENT_DATE,
	null,
	profeid,
	null);

END //

CREATE PROCEDURE esborrarProveidor(IN proid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Proveidors
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = proid;

END //

CREATE PROCEDURE modificarProveidor(IN proid SMALLINT(5) UNSIGNED, IN nom_proveidor VARCHAR(30), IN descr VARCHAR(100), IN dir VARCHAR(100), IN cp INTEGER(10) UNSIGNED, IN obs VARCHAR(100))
BEGIN

UPDATE
	Proveidors
SET
	Nom = nom_proveidor,
	Descripcio = descr,
	Direccio = dir,
	CodiPostal = cp,
	Observacions = obs
WHERE
	ID = proid;

END //

CREATE PROCEDURE llistarProveidors()
BEGIN

SELECT
	pd.ID,
	pd.Nom 'Nom empresa',
	Descripcio,
	Direccio,
	po.Nom 'nomPoblacio',
	pd.CodiPostal,
	pv.Nom 'nomProvincia',
	Observacions
FROM
	Proveidors pd,
	Provincies pv,
	Poblacions po,
	CodisPostals cp
WHERE
	cp.Poblacio = po.ID AND
	po.Provincia = pv.ID AND
	pd.CodiPostal = cp.ID AND
	pd.DataBaixa is null;

END //

DELIMITER ;


