USE inventari;

DROP PROCEDURE IF EXISTS selectAllContenidors;
DROP PROCEDURE IF EXISTS selectAllContenidors_departament;
DROP PROCEDURE IF EXISTS selectAllContenidors_cicle;
DROP PROCEDURE IF EXISTS selectAllContenidors_modul;
DROP PROCEDURE IF EXISTS selectAllContenidors_uf;
DROP PROCEDURE IF EXISTS selectUnContenidor;
DROP PROCEDURE IF EXISTS inserirContenidor;
DROP PROCEDURE IF EXISTS selectTipusContenidors;
DROP PROCEDURE IF EXISTS modificarContenidor;
DROP PROCEDURE IF EXISTS esborrarContenidor;
DROP PROCEDURE IF EXISTS assignarContenidorAUF;
DROP PROCEDURE IF EXISTS esborrarContenidorDeUF;

DELIMITER //

CREATE PROCEDURE assignarContenidorAUF(in ufid INTEGER, IN idContenidor SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Contenidors_vs_UFs
VALUES
	(ufid,idContenidor);

END //

CREATE PROCEDURE esborrarContenidorDeUF(in idContenidor INTEGER)
BEGIN

DELETE FROM
	Contenidors_vs_UFs
WHERE
	Contenidor = idContenidor;

END //

CREATE PROCEDURE esborrarContenidor(IN idContenidor INTEGER, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Contenidor
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = idContenidor;

DELETE FROM
	Contenidors_vs_UFs
WHERE
	Contenidor = idContenidor;

CALL esborrarContenidorDeUF(idContenidor);

END //

CREATE PROCEDURE modificarContenidor(IN idContenidor INTEGER, IN tipusc TINYINT(3) UNSIGNED, IN idf VARCHAR(30), IN ub VARCHAR(40), IN val FLOAT, IN dc VARCHAR(10), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Contenidor
SET
	Tipus = tipusc,
	identificador = idf,
	Ubicacio = ub,
	Valor = val,
	DataCaducitat = STR_TO_DATE(dc, '%d-%m-%Y')
WHERE
	ID = idContenidor;

END //

CREATE PROCEDURE selectTipusContenidors()
BEGIN

SELECT
	ID,
	Nom
FROM
	TipusContenidor;

END //

CREATE PROCEDURE selectAllContenidors(IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.Nom ,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	Contenidors_vs_UFs cu,
	UFs uf,
	Professor_vs_UFs pu,
	TipusContenidor tc
WHERE
	pu.UF = uf.ID AND
	c.ID = cu.Contenidor AND
	uf.ID = cu.UF AND
	pu.Professor = profeid AND
	tc.ID = c.Tipus

UNION

SELECT
	c.ID,
	c.Tipus,
	tc.Nom ,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	TipusContenidor tc
WHERE
	tc.ID = c.Tipus AND
	c.ID not in (
		SELECT
			cu.Contenidor
		FROM
			Contenidors_vs_UFs cu		
	);

END //

CREATE PROCEDURE selectAllContenidors_departament(IN deptid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.Nom,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	Contenidors_vs_UFs cu,
	Moduls mo,
	Cicles ci,
	Departaments de,
	TipusContenidor tc,
	Professor_vs_UFs pu,
	UFs uf
WHERE
	de.ID = deptid AND
	de.ID = ci.Departament AND
	ci.ID = mo.Cicle AND
	mo.ID = cu.UF AND
	cu.Contenidor = c.ID AND
	tc.ID = c.Tipus AND
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	uf.ID = cu.UF;

END //

CREATE PROCEDURE selectAllContenidors_cicle(IN cicleid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.Nom,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	Contenidors_vs_UFs cu,
	Moduls mo,
	Cicles ci,
	UFs uf,
	Professor_vs_UFs pu,
	TipusContenidor tc
WHERE
	ci.ID = cicleid AND
	ci.ID = mo.Cicle AND
	mo.ID = cu.UF AND
	cu.Contenidor = c.ID AND
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	uf.ID = cu.UF AND
	tc.ID = c.Tipus;

END //

CREATE PROCEDURE selectAllContenidors_modul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.Nom,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	Contenidors_vs_UFs cu,
	Moduls mo,
	UFs uf,
	Professor_vs_UFs pu,
	TipusContenidor tc
WHERE
	mo.ID = modulid AND
	mo.ID = uf.Modul AND
	cu.Contenidor = c.ID AND
	tc.ID = c.Tipus AND
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	uf.ID = cu.UF AND
	c.DataBaixa is null;

END //

CREATE PROCEDURE selectAllContenidors_uf(IN ufid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.Nom,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	Contenidors_vs_UFs cu,
	TipusContenidor tc,
	Professor_vs_UFs pu,
	UFs uf
WHERE
	pu.UF = uf.ID AND
	cu.UF = ufid AND
	cu.Contenidor = c.ID AND
	uf.ID = cu.UF AND
	pu.Professor = profeid AND
	tc.ID = c.Tipus;

END //

CREATE PROCEDURE selectUnContenidor(IN contenidorid SMALLINT(5) UNSIGNED)
BEGIN

SELECT
	c.ID,
	c.Tipus,
	tc.ID,
	c.identificador,
	c.Ubicacio,
	c.Valor,
	c.DataAlta,
	c.DataCaducitat,
	c.DataBaixa,
	c.UsuariDonaAlta,
	c.UsuariDonaBaixa
FROM
	Contenidor c,
	TipusContenidor tc
WHERE
	c.ID = contenidorid AND
	tc.ID = c.Tipus;

END //

CREATE PROCEDURE inserirContenidor(IN tContenidor VARCHAR(30), IN idf VARCHAR(30), IN ub VARCHAR(40), IN valor FLOAT, IN dCaducitat VARCHAR(10), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Contenidor
VALUES
	(LAST_INSERT_ID(),
	(SELECT
		ID
	FROM
		TipusContenidor
	WHERE
		Nom = tContenidor
	),
	idf,
	ub,
	valor,
	CURRENT_DATE,
	STR_TO_DATE(dCaducitat, '%d-%m-%Y'),
	null,
	profeid,
	null);	

END //

DELIMITER ;
