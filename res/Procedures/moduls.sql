USE inventari;

DROP PROCEDURE IF EXISTS selectModuls;
DROP PROCEDURE IF EXISTS selectAllModuls;
DROP PROCEDURE IF EXISTS inserirModul;
DROP PROCEDURE IF EXISTS esborrarModul;
DROP PROCEDURE IF EXISTS modificarModul;
DROP PROCEDURE IF EXISTS esborrarModuls_cicle;
DROP PROCEDURE IF EXISTS esborrarModuls_departament;

DELIMITER //

CREATE PROCEDURE selectModuls(IN cicleid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	mo.ID,
	mo.Nomenclatura,
	mo.Nom
FROM
	Moduls mo,
	UFs uf,
	Professor_vs_UFs pu
WHERE
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	mo.Cicle = cicleid AND
	uf.Modul = mo.ID AND
	mo.DataBaixa is null;
END //

CREATE PROCEDURE selectAllModuls(IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	mo.ID,
	mo.Nomenclatura,
	mo.Nom
FROM
	UFs uf,
	Professor_vs_UFs pu,
	Moduls mo
WHERE
	pu.UF = uf.ID AND
	mo.ID = uf.Modul AND
	pu.Professor = profeid AND
	mo.DataBaixa is null;
END //

CREATE PROCEDURE inserirModul(IN cicleid TINYINT(3) UNSIGNED, IN Nomenclatura VARCHAR(5), IN Nom VARCHAR(50), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Moduls
VALUES
	(LAST_INSERT_ID(),
	cicleid,
	Nomenclatura,
	Nom,
	CURRENT_DATE,
	null,
	profeid,
	null);

END //

CREATE PROCEDURE esborrarModul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE 
	Moduls
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = modulid;

CALL esborrarUFs_modul(modulid,profeid);

END //

CREATE PROCEDURE esborrarModuls_cicle(IN cicleid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Moduls
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	Cicle = cicleid;

END //

CREATE PROCEDURE esborrarModuls_departament(IN deptid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Moduls
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	Cicle in (
		SELECT
			ID
		FROM
			Cicles
		WHERE
			Departament = deptid
	);

END //

CREATE PROCEDURE modificarModul(IN modulid SMALLINT(3) UNSIGNED, IN cicleid TINYINT(3) UNSIGNED, IN nomcltr VARCHAR(5), nommodul VARCHAR(30), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Moduls
SET
	Cicle = cicleid,
	Nomenclatura = nomcltr,
	Nom = nommodul
WHERE
	ID = modulid;

END //

DELIMITER ;
