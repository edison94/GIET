USE inventari;

DROP PROCEDURE IF EXISTS selectAllMaterial;
DROP PROCEDURE IF EXISTS selectAllMaterial_departament;
DROP PROCEDURE IF EXISTS selectAllMaterial_cicle;
DROP PROCEDURE IF EXISTS selectAllMaterial_modul;
DROP PROCEDURE IF EXISTS selectAllMaterial_uf;
DROP PROCEDURE IF EXISTS selectAllMaterial_contenidor;
DROP PROCEDURE IF EXISTS inserirMaterial;
DROP PROCEDURE IF EXISTS selectUnMaterial;
DROP PROCEDURE IF EXISTS modificarMaterial;
DROP PROCEDURE IF EXISTS esborrarMaterial;
DROP PROCEDURE IF EXISTS assignarMaterialAContenidor;
DROP PROCEDURE IF EXISTS treureMaterialDeContenidor;
DROP PROCEDURE IF EXISTS llistarTipusMaterials;

DELIMITER //

CREATE PROCEDURE treureMaterialDeContenidor(IN materialid INTEGER)
BEGIN

DELETE FROM
	Material_vs_Contenidors
WHERE
	Material = materialid;

END //

CREATE PROCEDURE llistarTipusMaterials()
BEGIN

SELECT
	ID,
	Nom
FROM
	TipusMaterial;

END //

CREATE PROCEDURE assignarMaterialAContenidor (IN materialid INTEGER, IN contenidorid INTEGER)
BEGIN

INSERT INTO
	Material_vs_Contenidors
VALUES
	(materialid,contenidorid);

END //

CREATE PROCEDURE esborrarMaterial(IN materialid INTEGER, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Material
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = materialid;

CALL treureMaterialDeContenidor(materialid);

END //

CREATE PROCEDURE selectAllMaterial(IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm,
	Professor_vs_UFs pu,
	Material_vs_Contenidors mc,
	Contenidors_vs_UFs cu,
	UFs uf
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	uf.ID = pu.UF AND
	pu.Professor = profeid AND
	uf.ID = cu.UF AND
	cu.Contenidor = mc.Contenidor AND
	mc.Material = m.ID

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //


CREATE PROCEDURE selectUnMaterial(IN materialid INTEGER UNSIGNED)
BEGIN

SELECT
	m.ID,
	m.Nom 'NomMaterial',
	m.Descripcio,
	m.numSerie,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial',
	m.inventariable,
	m.identificador,
	m.DataCaducitat,
	m.valorPerUnitat,
	m.quantitatInicial,
	m.quantitatActual,
	m.quantitatMinima,
	pro.Nom 'Proveidor',
	m.DataGarantia,
	m.Observacions
FROM
	Material m
	JOIN (
		SELECT
			pro.ID,
			pro.Nom
		FROM
			Proveidors pro
	) pro ON (
		m.Proveidor = pro.ID
	) JOIN (
		SELECT
			tm.ID,
			tm.Nom
		FROM
			TipusMaterial tm
	) tm ON (
		m.tipusMaterial = tm.ID
	)
WHERE
	m.ID = materialid;

END //

CREATE PROCEDURE selectAllMaterial_contenidor(IN contenidorid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m
	JOIN (
		SELECT
			mc.Material,
			mc.Contenidor
		FROM
			Material_vs_Contenidors mc
		WHERE
			mc.Contenidor = contenidorid
	) mc ON (
		m.ID = mc.Material
	) JOIN (
		SELECT
			ID,
			Nom
		FROM
			TipusMaterial
	) tm ON (
		m.TipusMaterial = tm.ID
	)
WHERE
	m.DataBaixa is null

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //

CREATE PROCEDURE selectAllMaterial_modul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm,
	Professor_vs_UFs pu,
	Material_vs_Contenidors mc,
	Contenidors_vs_UFs cu,
	UFs uf,
	Moduls mo
WHERE
	mo.ID = modulid AND
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	mo.ID = uf.ID AND
	uf.ID = pu.UF AND
	pu.Professor = profeid AND
	uf.ID = cu.UF AND
	cu.Contenidor = mc.Contenidor AND
	mc.Material = m.ID

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //

CREATE PROCEDURE selectAllMaterial_cicle(IN cicleid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm,
	Professor_vs_UFs pu,
	Material_vs_Contenidors mc,
	Contenidors_vs_UFs cu,
	UFs uf,
	Moduls mo,
	Cicles ci
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	ci.ID = mo.Cicle AND
	mo.ID = uf.ID AND
	uf.ID = pu.UF AND
	pu.Professor = profeid AND
	uf.ID = cu.UF AND
	cu.Contenidor = mc.Contenidor AND
	mc.Material = m.ID AND
	ci.ID = cicleid

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //

CREATE PROCEDURE selectAllMaterial_departament(IN deptid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm,
	Professor_vs_UFs pu,
	Material_vs_Contenidors mc,
	Contenidors_vs_UFs cu,
	UFs uf,
	Moduls mo,
	Cicles ci,
	Departaments de
WHERE
	de.ID = deptid AND
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	de.ID = ci.Departament AND
	ci.ID = mo.Cicle AND
	mo.ID = uf.ID AND
	uf.ID = pu.UF AND
	pu.Professor = profeid AND
	uf.ID = cu.UF AND
	cu.Contenidor = mc.Contenidor AND
	mc.Material = m.ID

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial'
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //

CREATE PROCEDURE selectAllMaterial_uf(IN ufid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial',
	m.DataBaixa
FROM
	Material m,
	TipusMaterial tm,
	Professor_vs_UFs pu,
	Material_vs_Contenidors mc,
	Contenidors_vs_UFs cu,
	UFs uf
WHERE
	uf.ID = ufid AND
	m.tipusMaterial = tm.ID AND
	uf.ID = pu.UF AND
	pu.Professor = profeid AND
	uf.ID = cu.UF AND
	cu.Contenidor = mc.Contenidor AND
	mc.Material = m.ID

UNION ALL

SELECT DISTINCT
	m.ID,
	m.Nom,
	m.Descripcio,
	m.Marca,
	m.Model,
	tm.Nom 'TipusMaterial',
	m.DataBaixa
FROM
	Material m,
	TipusMaterial tm
WHERE
	m.DataBaixa is null AND
	m.tipusMaterial = tm.ID AND
	m.ID not in (
		SELECT
			mc.Material
		FROM
			Material_vs_Contenidors mc
	);

END //

CREATE PROCEDURE inserirMaterial(IN Nom VARCHAR(50), IN Descripcio VARCHAR(100), IN numSerie VARCHAR(30), IN Marca VARCHAR(30), IN Model VARCHAR(20), IN tipusMaterial TINYINT(3) UNSIGNED, IN inventariable TINYINT(1), IN identificador VARCHAR(50), IN dataCaducitat DATE, IN valorPerUnitat FLOAT, IN quantitatInicial INTEGER, IN quantitatActual INTEGER, IN quantitatMinima INTEGER, IN Proveidor SMALLINT(5) UNSIGNED, IN dataGarantia DATE, IN Observacions VARCHAR(100), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Material
VALUES
	(LAST_INSERT_ID(), Nom, Descripcio, numSerie, Marca, Model, tipusMaterial, inventariable, identificador, dataCaducitat, valorPerUnitat, quantitatInicial, quantitatActual, quantitatMinima, Proveidor, dataGarantia, Observacions, CURRENT_DATE, null, profeid, null);	

END //

CREATE PROCEDURE modificarMaterial(IN idMaterial INTEGER UNSIGNED, IN nom_material VARCHAR(50), IN descr VARCHAR(100), IN nSerie VARCHAR(30), IN marca_material VARCHAR(30), IN model_material VARCHAR(20), IN tMaterial INTEGER, IN inv BOOLEAN, IN dCaducitat VARCHAR(10), IN valor FLOAT, IN qi INTEGER, IN qa INTEGER, IN qm INTEGER, IN idProveidor INTEGER, IN datag VARCHAR(10), IN obs VARCHAR(100), IN idf VARCHAR(50))
BEGIN

UPDATE
	Material m
SET
	Nom = CASE char_length(nom_material) 
		WHEN 0 then null
		ELSE nom_material
	END,
	Descripcio = CASE char_length(descr) 
		WHEN 0 then null
		ELSE descr
	END, 
	numSerie = CASE char_length(nSerie)
		WHEN 0 then null 
		ELSE nSerie
	END,
	Marca = CASE char_length(marca_material) 
		WHEN 0 then null 
		ELSE marca_material
	END,
	Model = CASE char_length(model_material) 
		WHEN 0 then null 
		ELSE model_material
	END, 
	tipusMaterial = tMaterial, 
	inventariable = inv, 
	identificador = CASE char_length(idf) 
		WHEN 0 then null 
		ELSE idf
	END,
	DataCaducitat = CASE char_length(dCaducitat)
			WHEN 0 then null
			ELSE STR_TO_DATE(dCaducitat, '%d-%m-%Y')
	END, 
	ValorPerUnitat = valor, 
	quantitatInicial = qi, 
	quantitatActual = qa, 
	quantitatMinima = qm, 
	Proveidor = idProveidor, 
	DataGarantia = CASE char_length(datag)
			WHEN 0 then null
			ELSE STR_TO_DATE(datag,'%d-%m-%Y')
	END, 
	Observacions = CASE char_length(obs) 
		WHEN 0 then null 
		ELSE obs
	END
WHERE
	ID = idMaterial;

END //

DELIMITER ;
