USE inventari;

DROP PROCEDURE IF EXISTS inserirNouUsuari;
DROP PROCEDURE IF EXISTS esborrarUsuari;
DROP PROCEDURE IF EXISTS modificarUsuari;
DROP PROCEDURE IF EXISTS canviarContrasenya;

DELIMITER //

CREATE PROCEDURE inserirNouUsuari(IN Permisos TINYINT(3) UNSIGNED, IN Nom VARCHAR(15), IN Cognoms VARCHAR(30), IN Email VARCHAR(50), IN Password VARCHAR(100), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Usuaris
VALUES
	(LAST_INSERT_ID(),
     	Permisos,
	Nom,
	Cognoms,
	Email,
	Password,
	CURRENT_DATE,
	null,
	profeid,
	null);

END //

CREATE PROCEDURE canviarContrasenya(IN pw VARCHAR(256), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Usuaris
SET
	Password = pw
WHERE
	ID = profeid;

END //


CREATE PROCEDURE esborrarUsuari(IN Usuari SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Usuaris
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = Usuari;

CALL treureProfessor(Usuari);
CALL treureCap_de_departament(Usuari);

END //

CREATE PROCEDURE modificarUsuari(IN userid SMALLINT(5) UNSIGNED, IN perm TINYINT(3) UNSIGNED, IN nomr VARCHAR(15), IN cr VARCHAR(30), IN mail VARCHAR(50), IN pw VARCHAR(100))
BEGIN

UPDATE
	Usuaris
SET
	Permisos = perm,
	Nom = nomr,
	Cognoms = cr,
	Email = mail,
	Password = pw
WHERE
	ID = userid;

END //

DELIMITER ;
