USE inventari;

DROP PROCEDURE IF EXISTS selectPermisos;
DROP PROCEDURE IF EXISTS selectDescripcio;

DELIMITER //

CREATE PROCEDURE selectPermisos()
BEGIN

SELECT
	ID,
	Nom,
	Comentaris
FROM
	Permisos;

END //

CREATE PROCEDURE selectDescripcio(IN permisid TINYINT(3) UNSIGNED)
BEGIN

SELECT
	Comentaris
FROM
	Permisos
WHERE
	ID = permisid;

END //

DELIMITER ;
