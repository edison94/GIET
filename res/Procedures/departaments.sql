USE inventari;

DROP PROCEDURE IF EXISTS selectDepartaments;
DROP PROCEDURE IF EXISTS crearDepartament;
DROP PROCEDURE IF EXISTS esborrarDepartament;
DROP PROCEDURE IF EXISTS modificarDepartament;
DROP PROCEDURE IF EXISTS llistarDepartaments;
DROP PROCEDURE IF EXISTS treureCap_de_departament;

DELIMITER //

CREATE PROCEDURE selectDepartaments(IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	de.ID,
    	de.Nom
FROM
	Cicles ci,
    	Moduls mo,
	Departaments de,
	UFs uf,
	Professor_vs_UFs pu
WHERE
	pu.UF = uf.ID AND
	pu.Professor = profeid AND
	mo.ID = uf.Modul AND
    	mo.Cicle = ci.ID AND
    	ci.Departament = de.ID AND
	de.DataBaixa is null;
END //

CREATE PROCEDURE crearDepartament(IN CapDepartament SMALLINT(5) UNSIGNED, Nom VARCHAR(30), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO 
	Departaments
VALUES (
	LAST_INSERT_ID(),
	CapDepartament,
	Nom,
	CURRENT_DATE,
	null,
	profeid,
	null
);

END //


CREATE PROCEDURE esborrarDepartament(IN deptid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE 
	Departaments
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = deptid;

CALL esborrarCicles_departament(deptid,profeid);
CALL esborrarModuls_departament(deptid,profeid);
CALL esborrarUFs_departament(deptid,profeid);

END //


CREATE PROCEDURE modificarDepartament(IN deptid TINYINT(3) UNSIGNED, IN cap SMALLINT(5) UNSIGNED, nomDepartament VARCHAR(30), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Departaments
SET
	CapDepartament = cap,
	Nom = nomDepartament
WHERE
	ID = deptid;

END //

CREATE PROCEDURE llistarDepartaments()
BEGIN

SELECT
	CapDepartament,
	Nom,
	DataAlta,
	DataBaixa,
	UsuariDonaAlta,
	UsuariDonaBaixa
FROM
	Departaments
WHERE
	DataBaixa is null;

END //

CREATE PROCEDURE treureCap_de_departament(IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Departaments
SET
	CapDepartament = null
WHERE
	CapDepartament = profeid;

END //

DELIMITER ;
