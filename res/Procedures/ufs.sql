USE inventari;

DROP PROCEDURE IF EXISTS selectUFs;
DROP PROCEDURE IF EXISTS selectAllUFs;
DROP PROCEDURE IF EXISTS selectAllUFs_modul;
DROP PROCEDURE IF EXISTS selectAllUFs_cicle;
DROP PROCEDURE IF EXISTS selectAllUFs_departament;
DROP PROCEDURE IF EXISTS inserirUF;
DROP PROCEDURE IF EXISTS esborrarUF;
DROP PROCEDURE IF EXISTS modificarUF;
DROP PROCEDURE IF EXISTS esborrarUFs_modul;
DROP PROCEDURE IF EXISTS esborrarUFs_cicle;
DROP PROCEDURE IF EXISTS esborrarUFs_departament;
DROP PROCEDURE IF EXISTS selectUFs_sense_professor;
DROP PROCEDURE IF EXISTS treureProfessor;
DROP PROCEDURE IF EXISTS treureProfessor_departament;
DROP PROCEDURE IF EXISTS treureProfessor_cicle;
DROP PROCEDURE IF EXISTS treureProfessor_modul;
DROP PROCEDURE IF EXISTS treureProfessor_uf;

DELIMITER //

CREATE PROCEDURE selectUFs(IN modulid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	uf.ID,
	uf.Nomenclatura,
	uf.Nom
FROM
	UFs uf,
	Professor_vs_UFs pu
WHERE
	pu.Professor = profeid AND
	uf.ID = pu.UF AND
	uf.Modul = modulid AND
	uf.DataBaixa is null;
END //

CREATE PROCEDURE selectAllUFs(IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	uf.ID,
	uf.Nomenclatura,
	uf.Nom
FROM
	UFs uf,
	Professor_vs_UFs pu
WHERE
	pu.Professor = profeid AND
	pu.UF = uf.ID;
END //

CREATE PROCEDURE selectAllUFs_modul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	uf.ID,
	uf.Nomenclatura,
	uf.Nom
FROM
	Moduls mo,
	UFs uf,
	Professor_vs_UFs pu
WHERE
	mo.ID = modulid AND
	pu.UF = uf.ID AND
	pu.Professor = profeid AND
	mo.ID = uf.Modul AND
	uf.DataBaixa is null;

END //

CREATE PROCEDURE selectAllUFs_cicle(IN cicleid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	uf.ID,
	uf.Nomenclatura,
	uf.Nom
FROM
	Cicles ci,
	Moduls mo,
	UFs uf,
	Professor_vs_UFs pu
WHERE
	ci.ID = cicleid AND
	pu.UF = uf.ID AND
	pu.Professor = profeid AND
	ci.ID = mo.Cicle AND
	mo.ID = uf.Modul AND
	uf.DataBaixa is null;

END //

CREATE PROCEDURE selectAllUFs_departament(IN deptid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

SELECT DISTINCT
	uf.ID,
	uf.Nomenclatura,
	uf.Nom
FROM
	Departaments de,
	Cicles ci,
	Moduls mo,
	Professor_vs_UFs pu,
	UFs uf
WHERE
	de.ID = deptid AND
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	ci.ID = mo.Cicle AND
	de.ID = ci.Departament AND
	mo.ID = uf.Modul AND
	uf.DataBaixa is null;

END //

CREATE PROCEDURE inserirUF(IN Professor SMALLINT(5) UNSIGNED, IN modulid TINYINT(3) UNSIGNED, IN Nomenclatura VARCHAR(5), IN Nom VARCHAR(50), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	UFs
VALUES
	(LAST_INSERT_ID(),
	Professor,
	modulid,
	Nomenclatura,
	Nom,
	CURRENT_DATE,
	null,
	profeid,
	null);

END //

CREATE PROCEDURE esborrarUF(IN ufid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE 
	UFs
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = ufid;

END //

CREATE PROCEDURE esborrarUFs_modul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	UFs
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	Modul = modulid;

END //

CREATE PROCEDURE esborrarUFs_cicle(IN cicleid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	UFs
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	Modul in (
		SELECT
			ID
		FROM
			Moduls
		WHERE
			Cicle = cicleid
	);

END //

CREATE PROCEDURE esborrarUFs_departament(IN deptid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	UFs
SET
	DataBaixa = CURRENT_DATE, 
	UsuariDonaBaixa = profeid
WHERE
	Modul in (
		SELECT
			ID
		FROM
			Moduls
		WHERE
			Cicle in (
				SELECT
					ID
				FROM
					Cicles
				WHERE
					Departament = deptid
			)
	);

END //

CREATE PROCEDURE modificarUF(IN ufid SMALLINT(3) UNSIGNED, IN pr SMALLINT(5) UNSIGNED, IN modulid TINYINT(3) UNSIGNED, IN nomcltr VARCHAR(5), nomuf VARCHAR(30), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	UFs
SET
	Modul = modulid,
	Nomenclatura = nomcltr,
	Nom = nomuf,
	Professor = pr
WHERE
	ID = ufid;

END //

CREATE PROCEDURE selectUFs_sense_professor()
BEGIN

SELECT
	Nomenclatura,
	Nom
FROM
	UFs uf
WHERE
	uf.ID not in (
		SELECT DISTINCT
			UF
		FROM
			Professor_vs_UFs pu
	) AND
	DataBaixa is null;

END //

CREATE PROCEDURE treureProfessor_uf(IN ufid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

DELETE FROM
	Professor_vs_UFs
WHERE
	Professor = profeid AND
	UF = ufid;

END //

CREATE PROCEDURE treureProfessor_modul(IN modulid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

DELETE FROM
	Professor_vs_UFs
WHERE
	Professor = profeid AND
	UF in (
		SELECT
			uf.ID
		FROM
			UFs uf,
			Moduls mo
		WHERE
			uf.Modul = mo.ID AND
			mo.ID = modulid
	);

END //

CREATE PROCEDURE treureProfessor_cicle(IN cicleid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

DELETE FROM
	Professor_vs_UFs
WHERE
	Professor = profeid AND
	UF in (
		SELECT
			uf.ID
		FROM
			UFs uf,
			Moduls mo,
			Cicles ci
		WHERE
			uf.Modul = mo.ID AND
			mo.Cicle = ci.ID AND
			ci.ID = cicleid
	);

END //

CREATE PROCEDURE treureProfessor_departament(IN deptid SMALLINT(5) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

DELETE FROM
	Professor_vs_UFs
WHERE
	Professor = profeid AND
	UF in (
		SELECT
			uf.ID
		FROM
			UFs uf,
			Moduls mo,
			Cicles ci,
			Departaments de
		WHERE
			uf.Modul = mo.ID AND
			mo.Cicle = ci.ID AND
			ci.Departament = de.ID AND
			de.ID = deptid
	);

END //

CREATE PROCEDURE treureProfessor(IN profeid SMALLINT(5) UNSIGNED)
BEGIN

DELETE FROM
	Professor_vs_UFs
WHERE
	Professor = profeid;

END //

DELIMITER ;
