USE inventari;

DROP PROCEDURE IF EXISTS selectCicles;
DROP PROCEDURE IF EXISTS selectAllCicles;
DROP PROCEDURE IF EXISTS crearCicle;
DROP PROCEDURE IF EXISTS esborrarCicle;
DROP PROCEDURE IF EXISTS modificarCicle;
DROP PROCEDURE IF EXISTS esborrarCicles_departament;

DELIMITER //

CREATE PROCEDURE selectCicles(IN deptid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	ci.ID,
	ci.Nomenclatura,
	ci.Nom
FROM
	Cicles ci,
	Moduls mo,
	UFs uf,
	Professor_vs_UFs pu
WHERE
	pu.Professor = profeid AND
	pu.UF = uf.ID AND
	ci.Departament = deptid AND
	uf.Modul = mo.ID AND
	mo.Cicle = ci.ID AND
	ci.DataBaixa is null;
END //

CREATE PROCEDURE selectAllCicles(IN profeid SMALLINT(5) UNSIGNED)
BEGIN
SELECT DISTINCT
	ci.ID,
	ci.Nomenclatura,
	ci.Nom
FROM
	UFs uf,
	Moduls mo,
	Cicles ci,
	Professor_vs_UFs pu
WHERE
	uf.ID = pu.UF AND
	ci.ID = mo.Cicle AND
	mo.ID = uf.Modul AND
	pu.Professor = profeid AND
	ci.DataBaixa is null;
END //

CREATE PROCEDURE crearCicle(IN deptid TINYINT(3) UNSIGNED, IN Nomenclatura VARCHAR(4), IN Nom VARCHAR(50), IN profeid SMALLINT(5) UNSIGNED)
BEGIN

INSERT INTO
	Cicles
VALUES
	(LAST_INSERT_ID(),
	deptid,
	Nomenclatura,
	Nom,
	CURRENT_DATE,
	null,
	profeid,
	null);

END //

CREATE PROCEDURE esborrarCicle(IN cicleid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Cicles
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	ID = cicleid;

CALL esborrarModuls_cicle(cicleid,profeid);
CALL esborrarUFs_cicle(cicleid,profeid);

END //

CREATE PROCEDURE esborrarCicles_departament(IN deptid TINYINT(3) UNSIGNED, IN profeid SMALLINT(5) UNSIGNED)
BEGIN

UPDATE
	Cicles
SET
	DataBaixa = CURRENT_DATE,
	UsuariDonaBaixa = profeid
WHERE
	Departament = deptid;

CALL esborrarModuls_departament(deptid,profeid);
CALL esborrarUFs_departament(deptid,profeid);

END //

CREATE PROCEDURE modificarCicle(IN cicleid TINYINT(3) UNSIGNED, IN deptid TINYINT(3) UNSIGNED, IN nomcltr VARCHAR(4), IN nom_cicle VARCHAR(50))
BEGIN

UPDATE
	Cicles
SET
	Departament = deptid,
	Nomenclatura = nomcltr,
	Nom = nom_cicle
WHERE
	ID = cicleid;

END //


DELIMITER ;
