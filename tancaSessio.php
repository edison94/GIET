<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");

if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<head>	
    <title>GIET - Sessi� tancada</title>
    <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".btn").click(function () {
                window.location.href = "myPage.php";
            });
        });
    </script>
</head>
<body>
    <div class="main">
        <div class="container">
            <div id="divError" class="jumbotron gris">
                <h1>Sessió tancada<small></h1>
                <br />
                <p>La sessió ha sigut tancada correctament<b></b> </p>
                <p>Per tornar a gestionar i utilitzar la aplicació GIET torna a la pagina de identificació per iniciar la sessió o simplement prem el següent botó:</p>
                <?php
                session_unset();
                session_destroy();
                @mysqli_close($conn);
                ?>
                <a class="btn btn-primary"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicia la sessió</a>
            </div>
        </div>
    </div>
</body>