<?php
@session_start();
@include 'recargaSesion.php';

$conn = new mysqli("localhost", $_SESSION['roleId'], "12345", "inventari");

if ($conn->connect_error) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>GIET - Contenidors</title>
       <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
        <link href="css/estilo.css" rel="stylesheet" type="text/css">
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" ></script>
        <script src="js/myJs.js"></script>      
        <!--<script src="bootstrap/js/modal.js"></script>-->
        <script src="bootstrap/dist/js/bootstrap-checkbox.min.js" defer></script>        
        <script type="text/javascript" src="bootstrap/moment/moment.js"></script>
        <script type="text/javascript" src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/transition.js"></script>
        <script type="text/javascript" src="bootstrap/js/collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //CARGAR COMBOS
                cambioDepartamento();
                llenarComboTipus();
                //PREPARAR BOTON BUSQUEDA TABLA
                $(".search").keyup(function () {
                    busquedaTabla(this)()
                });
                $("#afegirContenidor").click(function () {
                    insertContenidor();
                });
                $("#siTancaSessio").click(function () {
                    window.location.href = "tancaSessio.php";
                });
                $("#canviaContrasenya").click(function () {
                    cambiarContrasena();
                });

            });
//
        </script>

    </head>
    <body>   
        <!--MODALES -->




        <!--MODIFICAR -->
        <div class="modal fade" id="modificarContenidorModal" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Modificar Contenidor</h3>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="idModificar" val="" />
                        <form action="" method="">
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Tipus</label>
                                <div class=" col-md-4" >
                                    <select class="form-control" type="text"  id="tipusContenidor"  name="tipusContenidor"> 
                                    </select>
                                </div>

                                <label class="col-md-2">Ubicació</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="ubicacioContenidor" name="ubicacioContenidor">                             
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Valor</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="valorContenidor" name="valorContenidor">           
                                </div>
                                <label class="col-md-2">Data Caducitat</label>
                                <div class=" col-md-4 " >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadContenidor" name="dataCadContenidor"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2" >Identificador</label>
                                <div class=" col-md-4" >
                                    <input class="form-control col-md-4" type="text" id="identificadorContenidor"  name="identificadorContenidor">                 
                                </div>
                            </div>
                            <label>Contingut: </label>
                            <!--Tabla del modal -->
                            <div class="combo-group contenido col-md-12 ">
                                <div id="divSearch" class="form-group pull-right" hidden>
                                    <input  id="search" type="text" class="search form-control" placeholder="Cerca">
                                </div>
                                <span class="counter pull-right"></span>
                                <table id="consulta" class="table table-hover table-striped table-bordered">
                                    <thead id="headConsulta">                   
                                    </thead>
                                    <tbody id="bodyConsulta">                    
                                    </tbody>
                                </table>
                            </div>   
                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <span onClick="modificaContenidor()" id="modificarContenidor" class="btn  btn-primary" data-action="save" role="button">Modificar</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 



        <!--AFEGIR -->

        <div class="modal fade" id="afegirContenidorModal" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Afegir Contenidor</h3>
                    </div>
                    <div class="modal-body">
                        <form action="" method="">
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Tipus</label>
                                <div class=" col-md-4" >

                                    <select class="form-control" type="text"  id="tipusContenidorAfegir"  name="tipusContenidorAfegir"> 
                                    </select>
                                </div>

                                <label class="col-md-2">Ubicació</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="ubicacioContenidorAfegir" name="ubicacioContenidorAfegir">                             
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Valor</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="valorContenidorAfegir" name="valorContenidorAfegir">           
                                </div>
                                <label class="col-md-2">Data Caducitat</label>
                                <div class=" col-md-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadContenidorAfegir" name="dataCadContenidorAfegir"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Identificador</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="identificadorContenidorAfegir" name="identificadorContenidorAfegir">           
                                </div>
                            </div>

                            <!--Tabla del modal -->
                            <div class="combo-group contenido col-md-12 ">
                                <div id="divSearch" class="form-group pull-right" hidden>
                                    <input  id="search" type="text" class="search form-control" placeholder="Cerca">
                                </div>
                                <span class="counter pull-right"></span>
                                <table id="consulta" class="table table-hover table-striped table-bordered">
                                    <thead id="headConsulta">                   
                                    </thead>
                                    <tbody id="bodyConsulta">                    
                                    </tbody>
                                </table>
                            </div>   
                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <span type="submit" id="afegirContenidor" class="btn  btn-primary" data-action="save" role="button">Afegir</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 


        <!--CONSULTAR -->

        <div class="modal fade" id="consultarContenidorModal" aria-hidden="true">
            <div class="modal-dialog modalModificar">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Consultar Contenidor</h3>
                    </div>
                    <div class="modal-body">
                        <form action="" method="">
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Tipus</label>
                                <div class=" col-md-4" >

                                    <select class="form-control" type="text"  id="tipusContenidorConsultar" disabled> 
                                    </select>
                                </div>

                                <label class="col-md-2">Ubicació</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="ubicacioContenidorConsultar"disabled >                             
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Valor</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="valorContenidorConsultar" disabled>           
                                </div>
                                <label class="col-md-2">Data Caducitat</label>
                                <div class=" col-md-4" >
                                    <div class='input-group date'>
                                        <input class="form-control datetimepicker1" id="dataCadContenidorConsultar" disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-2">Identificador</label>
                                <div class=" col-md-4" >
                                    <input type="text" class="form-control" id="identificadorContenidorConsultar"disabled >           
                                </div>
                            </div>

                            <label>Contingut: </label>
                            <!--Tabla del modal -->
                            <div class="combo-group contenido col-md-12 ">
                                <div id="divSearchConsultar" class="form-group pull-right" hidden>
                                    <input  id="searchConsultar" type="text" class="search form-control" placeholder="Cerca">
                                </div>
                                <span class="counter pull-right"></span>
                                <table id="consultaConsultar" class="table table-hover table-striped table-bordered">
                                    <thead id="headConsultaConsultar">                   
                                    </thead>
                                    <tbody id="bodyConsultaConsultar">                    
                                    </tbody>
                                </table>
                            </div>  
                            <div class="modal-footer">
                                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <!-- line modal -->
        <div class="modal fade" id="eliminarContenidor" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Alerta!</h3>
                    </div>
                    <div class="modal-body">

                        <!-- content goes here -->
                        <span>Segur que vols eliminar aquesta informació?</span>
                        </br>
                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">No</button>
                        <span type="button" onClick="eliminaContenidorConfirmado()" id="eliminarContenidor" class="btn  btn-primary"  role="button">Si</span>
                    </div>
                </div>
            </div>
            <input id="eliminaContenidor"type="hidden" val=""/>
        </div>
        <div class="modal fade" id="confirmTancarSessio" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Segur que vols tancar la sessió?</h3>
                    </div>
                    <div class="modal-body">
                        <!-- content goes here -->
                        <span>Tingues en compte que si tanques la sessió, ja no podràs continuar navegant</span>  </br>     </br>                      
                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">No</button>
                        <button type="button" id="siTancaSessio" class="btn  btn-primary"  role="button">Si</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="canviaPassModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="lineModalLabel">Canviar la contrasenya</h3>
                    </div>
                    <div>
                        <!-- content goes here -->
                        <div id="infoPass"></div>
                        <div class="form-group col-md-12 col-sm-12 firstGroup">
                            <label class="col-md-4 col-sm-4">Nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass1" name="newPass1">                             
                            </div>
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label class="col-md-4 col-sm-4">Confirmar nova contrasenya</label>
                            <div class=" col-md-8 col-sm-8" >
                                <input type="text" class="form-control" id="newPass2" name="newPass2">           
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Tancar</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <span id="canviaContrasenya" type="submit" class="btn btn-primary" data-action="save" role="button">Canvia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--MODALES FIN -->    

        <!--HEADER-->
        <div class="banner">
            <div class="bienvenida">
                <?php
                echo 'Benvingut, ' . utf8_encode($_SESSION['nom']) . ' ' . utf8_encode($_SESSION['cognom']);
                if (isset($_SESSION['noHash'])) {
                    echo "<div class='alert alert-danger' role='alert'>CONTRASEÑA NO HASHEADA</div>";
                }
                ?>
            </div>
            <div>        
                <img src="img/giet.png"/>
            </div>      
        </div>
        <nav class="nav nav-tabs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="material.php"><span>Material</span></a></li>                        
                        <li class="altaUsuario"><a href="registro.php"><span>Usuaris</span></a></li>
                        <li class="active"><a href="contenidors.php"><span>Contenidors</span></a></li>
                        <li class="asignar"><a href="AsignarMaterial.php"><span>Assignar material</span></a></li>
                        <li class="asignar"><a href="AsignarContenedor.php"><span>Assignar Contenidors</span></a></li>
                    </ul>
                    <div class="pull-right">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> El meu compte<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a onclick="limpiaModalCTS()" href="#" data-toggle="modal" data-target="#canviaPassModal"><i class="icon-cog"></i>Canviar la contrasenya</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" data-toggle="modal" data-target="#confirmTancarSessio"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Tancar la sessió</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!--FIN HEADER-->

        <!--SELECTS-->
        <div class="contenidoLabels col-md-12 col-sm-12">                           
            <div class="col-md-2 col-sm-2">Departament</div>            
            <div class="col-md-2 col-sm-2">Cicles</div>            
            <div class="col-md-2 col-sm-2">Moduls</div>            
            <div class="col-md-2 col-sm-2">UFs</div>            
        </div>
        <div class="contenidoSelects combo-group col-md-12 col-sm-12">                           
            <!--ComboBox de Departamento -->
            <div class="col-md-2 col-sm-2">                    
                <select id="departamento" onChange="cambioCiclo()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de Ciclos -->
            <div class="col-md-2 col-sm-2">   
                <select id="ciclo" onChange="cambioModulo()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de Modulos -->
            <div class="col-md-2 col-sm-2">   
                <select id="modulo" onChange="cambioUF()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>
            <!--ComboBox de UFS -->
            <div class="col-md-2 col-sm-2">   
                <select id="UF" onChange="busquedaLista()" class="form-control">
                    <option>Sense resultats</option>
                </select>
            </div>

            <div class="col-md-2">
                <button onClick="construirTablaContenidor()" id="btnBuscarContenidors" type="submit" class="btn btn-primary" disabled>Busca</button></span>
            </div>

            <!--Boton que presentara la tabla-->
            <!--                <div class="col-md-2">   
                                <button disabled id="buscar" onClick="construirTabla()">Buscar</button>
            <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
                            </div>-->
        </div>

        <!--FIN SELECTS-->
        <!-- BOTON AÑADIR CONTENEDOR -->
        <div>
            <button id="anadirContenedor" class="btn-primary anadir col-md-12 " data-toggle="modal" data-target="#afegirContenidorModal" href="path/to/settings" aria-label="Delete">
                Afegir Contenidor
            </button>
        </div>
        <!--TABLA-->
        <div class="combo-group contenido col-md-12 ">
             <div id="infoDiv"></div>
            <div id="divSearchContenidor" class="form-group pull-right" hidden>
                <input  id="search" type="text" class="search form-control" placeholder="Cerca">
            </div>
            <span class="counter pull-right"></span>
            <table id="consultaContenidor" class="table table-hover table-striped table-bordered">
                <thead id="headConsultaContenidor">                   
                </thead>
                <tbody id="bodyConsultaContenidor">                    
                </tbody>
            </table>
        </div>   
        <!--FIN TABLA-->

    </body>
</html>