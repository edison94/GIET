<!DOCTYPE html>
<head>	
    <title>GIET - Inici</title>
  <link rel="shortcut icon" href="/proyectoFinal/favicon.ico" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>	
</head>
<body>
    <div class="main">   
        <div class="container">
            <?php conexion() ?>
            <center>             
                <div class="middle">
                    <div id="login">
                        <form method="POST">
                            <fieldset class="clearfix">
                                <p><span class="fa fa-user"></span>
                                    <input type="text" name="userName" id="userName" Placeholder="Usuari" required></p>
                                <p><span class="fa fa-lock"></span>
                                    <input type="password" name="userPassword" Placeholder="Contrasenya" required></p>
                                <div>
                                    <span style="width:50%; text-align:right;  display: inline-block;"><input type="submit" name="d" value="Inicia la sessió"></span>
                                </div>
                            </fieldset>
                            <div class="clearfix"></div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="logo">
                        <div class="clearfix"><img src="img/logo2.png"/></div>
                    </div>
                </div>
            </center>
        </div>
    </div>
</body>

<?php

function conexion() {
    if (isset($_POST['userName'])) {

        $host_db = "localhost";
        $user_db = trim($_POST['userName']);
        $user_db = strip_tags($user_db);
        $user_db = htmlspecialchars($user_db);
        $pass_db = trim($_POST['userPassword']);
        $pass_db = strip_tags($pass_db);
        $pass_db = htmlspecialchars($pass_db);
        $db_name = "inventari";

        $conn = new mysqli("localhost", "always", "12345", "inventari");

        session_start();
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $user_db;
        $_SESSION['start'] = time();
        $_SESSION['expire'] = $_SESSION['start'] + (5 * 60);

        if (strpos($user_db, '@correu.escoladeltreball.org') == false) {
            $user_db = $user_db . '@correu.escoladeltreball.org';
        }

        $login = 'SELECT ID, Permisos FROM Usuaris WHERE Email=\'' . $user_db . '\'';
//        $login = 'CALL selectPermisos_ByMail(\'' . $user_db . '\')';
        $test = mysqli_query($conn, $login);

        $role = mysqli_query($conn, $login);

        if (mysqli_num_rows($test) < 1) {
            echo "<div class='alert alert-danger' role='alert'>L'usuari no existeix</div>";
        } else {
            $password = hash('sha256', $pass_db);
            //$passwod = md5($pass_db)
//            $_SESSION['noHash'] = "true";
//            $password = $pass_db;
            $login = 'SELECT ID, Permisos, Nom, Cognoms FROM Usuaris WHERE Email = \'' . $user_db . '\' AND Password = \'' . $password . '\'';
            //$login = 'CALL selectUser(\'' . $user_db . '\' , \'' . $password . '\')';	
		
            $role = mysqli_query($conn, $login);
            if (mysqli_num_rows($role) < 1) {
                echo "<div class='alert alert-danger' role='alert'>La contrasenya és incorrecta </div>";
            } else {
                $row = $role->fetch_assoc();
                $roleid = $row["Permisos"];
                $userid = $row["ID"];
                $nom = $row["Nom"];
                $cognom = $row["Cognoms"];

                $_SESSION['username'] = substr($user_db, 0, strpos($user_db, '@correu.escoladeltreball.org'));
                $_SESSION['userId'] = $userid;
                $_SESSION['roleId'] = $roleid;
//                $_SESSION['roleId'] = "always";
                $_SESSION['pass'] = $password;
                $_SESSION['nom'] = $nom;
                $_SESSION['cognom'] = $cognom;

                header('Location: material.php');
            }
        }
        @mysqli_close($conn);
    }
}
?>
